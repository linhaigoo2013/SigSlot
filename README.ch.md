
# 使用SigSlot

[中文](https://github.com/LegendJohna/SigSlot/blob/main/README.ch.md) || [English](https://github.com/LegendJohna/SigSlot/blob/main/README.md)

如果你有任何问题，可以尝试联系我 : ykikoykikoykiko@gmail.com

只需要包含头文件SigSlot.hpp,并且使用C++17就可以使用信号槽机制开始编程了

## 示例
```c++
class Window 
{  
public:  
    //使用Event关键字定义事件，
    //模板参数就是所要传递的参数
    SigSlot::Event<int, char, std::string> event;  
};
//辅助测试类
class Button
{  
    int id;
public:  
    Button(int value):id(value){}
    void OnClick(int a, char b, std::string c)  
    {  
        std::cout << "对象的ID是" << id << "参数类型是" << a << " " << b << " " << c << std::endl;  
    }  
};
```
```c++
int main()  
{  
    Window window;  
    Button button(3);  
    //使用connect进行回调函数的绑定
    //template<typename T, typename U, typename ...SignalArgs, typename ...SlotArgs>
    //void connect(T *sender, Event<SignalArgs...> T::* event, U *receiver, void(U::* func)(SlotArgs...), SIGSLOT type = SIGSLOT::AutoConnection);
    //有五个函数参数，第一个是事件的发出者，第二个是事件，第三个是信号的接收者，第四个是要绑定的函数，第五个是连接类型（后面回说）

    SigSlot::connect(&window, &Window::event, &button, &Button::OnClick);  
    //触发事件，触发所有回调函数
    window.event.emit(1, 'a', "hello");  
    return 0;  
    
}
```
上面只是一个简单的例子，似乎还没有使用回调函数简单，让我们接着往下看
```c++
class Label 
{  
public:  
    void TextChanged(std::string text)  
    {  
        std::cout << "Label" << text << std::endl;  
    }  
};

int main()  
{  
    Window window;  
    Button button;  
    Label label; 
    //支持一对多，多对一的连击方式 
    SigSlot::connect(&window, &Window::event, &button, &Button::OnClick);
    
    //支持传递的参数多于要触发的函数的参数
    //多余的参数通过模板元编程自动裁剪掉，只传递需要的参数
    //具体来说当函数参数是事件参数的子序列的时候，允许connect，否则抛出一条静态断言错误  
    SigSlot::connect(&window, &Window::event, &label, &Label::TextChanged);  
    window.event.emit(1, 'a', "hello");  
    //运行结果
    //Label hello
    //对象id是1，参数是1 a hello
    //不保证执顺序和连接顺序相同（为了提高效率，底层采用unordered_map实现）
	
    //支持断开连接
    SigSlot::disconnect(&window, &Window::event, &label, &Label::TextChanged);  
    window.event.emit(1, 'a', "hello");
	
    //支持lambda作为连接的函数
    SigSlot::connect(&window,&Window::event, &label,[&](std::string text)  
    {  
	std::cout << "Label " << text << std::endl;  
    });
    //lambda表达式不支持断开连接
    window.event.emit(1, 'a', "hello");
	
    //支持连接普通的全局函数(静态函数)
    //void Print(int a, char b)  
    //{  
    //    std::cout << "Print " << a << " " << b << " " << std::endl;  
    //}
    SigSlot::connect(window, &Window::event, Print);  
    std::cout << "------------------------\n";  
    window->event.emit(3, 'a', "func");  
    SigSlot::disconnect(window, &Window::event, Print);
    return 0;  
}
```
## 对象死亡自动断开连接
```c++
//解决访问已经死亡的对象的安全问题
int main()  
{  
    Window *window = new Window();  
    Button *button = new Button(3);  
    Label *label = new Label();  
    SigSlot::connect(window, &Window::event, button, &Button::OnClick);  
    SigSlot::connect(window, &Window::event, label, &Label::TextChanged);  
    SigSlot::connect(window, &Window::event, button, [](int a, std::string c)  
    {  
        std::cout << "Lambda " << a << " " << " " << c << std::endl;  
    });  
    window->event.emit(1, 'a', "hello");  
    //Lambda 1  hello
    //Labelhello
    //对象的ID是 1参数类型是1 a hello
    delete button;  
    window->event.emit(2, 'b', "world");
    //Lambda 2  world
    //Labelworld
    //对象的ID是 -778816336参数类型是2 b world  
    //可以发现访问了，已经被析构的对象的成员，这样是极其不安全的行为
    //为了避免这种情况用户需要手动断开订阅的事件
    //就像这样SigSlot::disconnect(window, &Window::event, button, &Button::OnClick);  
    delete window;  
    delete label;  
    return 0;  
}
//这样是可行的，但是太多了，未免不会太无聊，而且容易忘记这件事，如果你想要自动断开连接，也完全可以的
//我们提供两种方法
1：继承内置的Object类
class Button :public SigSlot::Object
{  
private:  
    int id;  
public:  
    Button(int value):id(value){}  
    void OnClick(int a, char b, std::string c)  
    {  
        std::cout << "对象的ID是 " << id << "参数类型是" << a << " " << b << " " << c << std::endl;  
    }  
};
2：使用我们预定义的宏
class Button 
{ 
    SIGSLOT_OBJECT 
private:  
    int id;  
public:  
    Button(int value):id(value){}  
    void OnClick(int a, char b, std::string c)  
    {  
        std::cout << "对象的ID是 " << id << "参数类型是" << a << " " << b << " " << c << std::endl;  
    }  
};
//不允许把一个自动断开连接的对象连接到一个非自动断开连接的对象
//如果你这样做了，会抛出一条静态编译错误
//"Sender and Receiver must both be Object or neither be"
//下面来看一下自动断开连接的效果

//给刚才的对象都加上SIGSLOT_OBJECT这个宏
//Lambda 1  hello
//Labelhello
//对象的ID是 1参数类型是1 a hello

//可以发现Button死亡的时候与之相关的连接被自动断开了
//Labelworld
```
## 多线程编程
```c++
//只有Object才能进行利用这个特性，即继承了Object或者使用了SIGSLOT_OBJECT这个宏
//这里写一个辅助函数来防止多线程下cout乱序的问题
template<typename ...Args>
void Print(Args&&... args)
{
    std::string str{};
    constexpr auto f = [&](auto arg)
    {
        if constexpr (std::is_same_v<decltype(arg), int>)
        {
            str += std::to_string(arg);
        }
        else if constexpr (std::is_same_v<decltype(arg), std::thread::id>)
        {
            std::stringstream ss{};
            ss << arg;
	    str += ss.str();
	}
        else
        {
            str += arg;
        }
    };
    (f(args), ...);
    std::cout << str;
}
```
```c++
class Window 
{  
    SIGSLOT_OBJECT
public:  
    SigSlot::Event<int, char, std::string> event;  
};  
  
class Button: public SigSlot::Object  
{  
public:  
    void OnClick(int a, char b, std::string c)  
    {  
        Print("Button ", a, " ", b, " ", c," 当前线程的ID是: ", std::this_thread::get_id(),"\n");  
    }  
    void Show(int a) const  
    {  
	Print("Button ", a, " 当前线程的ID是: ", std::this_thread::get_id(), "\n");  
    }
};  
class Label 
{  
    SIGSLOT_OBJECT
public:  
    void TextChanged(std::string text)  
    {  
        Print("Label ", text," 当前线程的ID是: ", std::this_thread::get_id(),"\n");  
    }  
};  
  
int main()  
{  
    std::thread t1([]()  
    {  
       //在子线程中开启事件循环
       SigSlot::EventLoop loop;  
       loop.Run();
       //为了可以方便的与别的库进行合作，你也可以写成下面这个形式
       //while(true)
       //{
       //	loop.Update();
       // } 
       //这样在别的库的事件循环里面也能处理信号槽的事件了
    });  
  
    std::this_thread::sleep_for(std::chrono::seconds(1));  
    Window *window = new Window();  
    Button *button = new Button();  
    Label *label = new Label(); 
    //将Button移动到子线程中，之后connect的Button的函数都在子线程中执行 
    button->MoveToThread(t1);  
    SigSlot::connect(window, &Window::event, button, &Button::OnClick);  
    SigSlot::connect(window, &Window::event, label, &Label::TextChanged);  
    SigSlot::connect(window, &Window::event, button, [](int a, std::string c)  
    {  
        Print("Button ", a, " ", c," 当前线程的ID是: ", std::this_thread::get_id(),"\n");  
    });  
    std::cout << "------------------------\n";  
    window->event.emit(1, 'a', "hello");  
    std::this_thread::sleep_for(std::chrono::seconds(2));  
    delete button;  
    delete window;  
    delete label;  
    t1.detach();  
    return 0;  
    //结果如下
    //------------------------
    //Label hello  当前线程的ID是: 1
    //Button 1 hello  当前线程的ID是: 2
    //Button 1 a hello  当前线程的ID是: 2
}
```
可以发现Button相关的函数都在子线程中执行了，而且是有序执行，后面会详细介绍
值得注意的是我们在上面分别进行了sleep的操作，第一个Sleep是确保子线程已经创建好了事件循环
第二个Sleep是确保在执行类的相关的函数的时候，对象仍然存活防止未定义的行为

## 更加全面的多线程支持
事实上connect是有第五个参数的
```c++
enum class SIGSLOT  
{  
    AutoConnection,  
    DirectConnection,  
    QueuedConnection,  
    BlockingQueuedConnection,  
};
```
四种可能的取值，首先要明确的是，在对象创建的时候默认是属于当前线程的，通过MoveToThread可以移动到别的线程，DirectConnection的意思就是事件触发的时候直接在当前线程执行函数，就是普通的回调函数的样字，QueuedConnection的意思是事件触发的时候，将函数打包成一个任务投送到对象所属于的线程的事件循环中，每个事件循环都有一个消息队列来存放消息，当消息队列中有消息的时候会不停的去除消息，并执行，没有消息就休眠线程并等待唤醒，可以发现QueuedConnection就可以实行异步的编程方式，假如需要做一些耗时间的工作，把任务投递到目标线程就行了，而且因为任务是在消息队列中一个个执行的，也不会有竞争的问题，Object就可以不用加锁了

而AutoConnection就是connect的默认参数，它的意思是，当对象线程属于当前的线程的时候会采用DirectConnection，不属于当前线程就使用QueuedConnection，而BlockingQueuedConnection的意思是，将任务投递到目标线程，并等待任务执行完毕再返回，其实就是异步阻塞式的编程方式，这四种方式已经覆盖了大部分的多线程需求了，只通过信号槽进行多线程编程是线程安全的，用户无需担心数据的竞争问题

## 手动投递任务
```c++
EventLoop *GetEventLoop(std::thread::id id = std::this_thread::get_id())
//通过这个函数可以获得目标线程的事件循环，注意事件循环一但创建会自动绑定当前线程
loop->PostEvent(std::function<void()>&& func);
loop->SendEvent(std::function<void()>&& func);
//通过这两种方式向目标线程投递任务
//Post是非阻塞式的，Send是阻塞式的
loop->Quit();
//退出事件循环
//另外由于标准C++的线程库，在线程初始化完成之前就可能继续向下执行导致在事件触发的时候EventLoop还没创建好,而且detach之后，尝试get_id()将会得到空值，为了解决这两个问题，简单对C++线程封装了一下，你可以这样使用
int main()  
{  
    SigSlot::Thread t1([]()  
    {  
        SigSlot::EventLoop loop;  
        loop.Run();  
    });  
    SigSlot::Thread t2([]()  
     {  
         SigSlot::EventLoop loop;  
         while(true)
         {
	     loop.Update();
         } 
     });  
    Window *window = new Window();  
    Button *button = new Button();  
    Label *label = new Label();  
    button->MoveToThread(t1);  
    label->MoveToThread(t2);  
    SigSlot::connect(window, &Window::event, button, &Button::Show,SigSlot::SIGSLOT::DirectConnection);  
    SigSlot::connect(window, &Window::event, button, &Button::OnClick,SigSlot::SIGSLOT::QueuedConnection);  
    SigSlot::connect(window, &Window::event, label, &Label::TextChanged,SigSlot::SIGSLOT::BlockingQueuedConnection);  
    std::cout << "------------------------\n";  
    window->event.emit(1, 'a', "hello");  
  
    delete button;  
    delete window;  
    delete label;  
    return 0;  
}
```
可以确保线程初始化完毕之后在往后执行
```c++
//测试结果
//Button 1  当前线程的ID是: 1
//Button 1 a hello  当前线程的ID是: 2
//Label hello  当前线程的ID是: 3
```
可以看到的确在目标线程执行了，这样就不会有数据竞争问题了

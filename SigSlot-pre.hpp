#ifndef SIGSLOT_SIGSLOT_HPP
#define SIGSLOT_SIGSLOT_HPP

#include <tuple>
#include <functional>
#include <type_traits>
#include <map>
#include <mutex>
#include <atomic>
#include <unordered_map>
#include <thread>
#include <mutex>
#include <atomic>
#include <functional>
#include <queue>
#include <optional>
#include <thread>
#include <condition_variable>
#include <shared_mutex>
#include <memory>
//Implementation namespace is used to hide the implementation details
//Implementation用于隐藏实现细节
namespace SigSlot::Implementation
{
    //select the parameters that function needs through template programming
    //通过模板元编程选择函数需要的参数

    template<typename Subset, typename Superset>
    struct is_subset_of;

    template<typename T, typename ...Subset, typename U, typename ...Superset>
    struct is_subset_of<std::tuple<T, Subset...>, std::tuple<U, Superset...>>
    {
        constexpr static bool value = is_subset_of<std::tuple<T, Subset...>, std::tuple<Superset...>>::value;
    };

    template<typename T, typename ...Subset, typename ...Superset>
    struct is_subset_of<std::tuple<T, Subset...>, std::tuple<T, Superset...>>
    {
        constexpr static bool value = is_subset_of<std::tuple<Subset...>, std::tuple<Superset...>>::value;
    };

    template<typename ...Superset>
    struct is_subset_of<std::tuple<>, std::tuple<Superset...>>
    {
        constexpr static bool value = true;
    };

    template<typename ...Subset>
    struct is_subset_of<std::tuple<Subset...>, std::tuple<>>
    {
        constexpr static bool value = false;
    };

    template<>
    struct is_subset_of<std::tuple<>, std::tuple<>>
    {
        constexpr static bool value = true;
    };

    template<int N, typename T, typename Tuple>
    struct find_next_index;

    template<int N, typename T, typename U, typename ...Args>
    struct find_next_index<N, T, std::tuple<U, Args...>>
    {
        constexpr static int value = find_next_index<N - 1, T, std::tuple<Args...>>::value + 1;
    };
    template<typename T, typename ...Args>
    struct find_next_index<-1, T, std::tuple<T, Args...>>
    {
        constexpr static int value = 0;
    };
    template<typename T, typename U, typename ...Args>
    struct find_next_index<-1, T, std::tuple<U, Args...>>
    {
        constexpr static int value = find_next_index<-1, T, std::tuple<Args...>>::value + 1;
    };

    template<int ...Args>
    struct int_list
    {
        using type = int_list<Args...>;

        template<typename Tuple>
        constexpr static auto MakeTupleByList(const Tuple &target) noexcept
        {
            return std::make_tuple(std::get<Args>(target)...);
        }
    };

    template<int N, typename list>
    struct list_prepend;

    template<int N, int ...Args>
    struct list_prepend<N, int_list<Args...>>
    {
        using result = int_list<N, Args...>;
    };

    template<int N, typename Subset, typename Superset>
    struct find_all_index;

    template<int N, typename T, typename ...Subset, typename ...Superset>
    struct find_all_index<N, std::tuple<T, Subset...>, std::tuple<Superset...>>
    {
        using value = typename list_prepend<find_next_index<N, T, std::tuple<Superset...>>::value,
                typename find_all_index<find_next_index<N, T, std::tuple<Superset...>>::value, std::tuple<Subset...>, std::tuple<Superset...>>::value>::result;
    };

    template<int N, typename ...Superset>
    struct find_all_index<N, std::tuple<>, std::tuple<Superset...>>
    {
        using value = int_list<>;
    };


    template<typename ...Subset, typename ...Superset>
    constexpr auto Apply(const std::function<void(Subset...)> &func, const std::tuple<Superset...> &target) noexcept
    {
        using SubsetTuple = std::tuple<std::decay_t<Subset>...>;
        using SupersetTuple = std::tuple<std::decay_t<Superset>...>;

        constexpr bool condition = is_subset_of<SubsetTuple, SupersetTuple>::value;
        static_assert(condition, "Handler function parameters and Event parameters do not match");

        if constexpr (condition)
        {
            using IndexList = typename find_all_index<-1, SubsetTuple, SupersetTuple>::value;
            std::apply(func, IndexList::MakeTupleByList(target));
        }
    }

    //get the parameter type of lambda
    //获取lambda的参数类型

    template<typename Lambda>
    struct lambda_traits;

    template<typename R,typename Lambda,typename ...Args>
    struct lambda_traits<R(Lambda::*)(Args...)>
    {
        using result_type = R;
        using function_type = std::function<R(Args...)>;
        using tuple_type = std::tuple<Args...>;
    };

    template<typename R,typename Lambda,typename ...Args>
    struct lambda_traits<R(Lambda::*)(Args...) const>
    {
        using result_type = R;
        using function_type = std::function<R(Args...)>;
        using tuple_type = std::tuple<std::decay_t<Args>...>;
    };

    //check if the type is object
    template<typename T, typename U>
    struct is_object
    {
        constexpr static bool value = false;
    };

    template<typename T>
    struct is_object<T, decltype(std::declval<T>().ThreadId())>
    {
        constexpr static bool value = true;
    };

    //to check all kinds of condition by enable_if
    template<typename Sender, typename Receiver, typename T, typename U, typename SignalTuple,typename SlotTuple>
    using CarrotClass = std::enable_if_t
    <
        Implementation::is_subset_of<SlotTuple,SignalTuple>::value
        && std::is_base_of_v<T,Sender> && std::is_base_of_v<U,Receiver>
    >;

    template<typename Sender, typename Receiver, typename T, typename Lambda, typename SignalTuple>
    using CarrotLambda = std::enable_if_t
    <
        Implementation::is_subset_of<typename Implementation::lambda_traits<decltype(&Lambda::operator())>::tuple_type,SignalTuple>::value
        && std::is_base_of_v<T,Sender>
    >;

    template<typename Sender,typename T,typename SignalTuple,typename SlotTuple>
    using GloablCarrot = std::enable_if_t
    <
        Implementation::is_subset_of<SlotTuple,SignalTuple>::value
        && std::is_base_of_v<T,Sender>
    >;
}

namespace SigSlot
{
    //declaration
    //声明
   enum SIGSLOT
   {
       AutoConnection,
       DirectConnection,
       QueuedConnection,
       BlockingQueuedConnection,
   };

   class Object;

   template<typename ...Args>
   class Event;

   class EventLoop;

   static EventLoop *GetEventLoop(std::thread::id id = std::this_thread::get_id());

   static EventLoop *GetEventLoop(const std::thread &thread);

    //connect member function
    //连接普通成员函数
    template<typename Sender, typename Receiver, typename T, typename U, typename ...SignalArgs, typename ...SlotArgs>
    static constexpr Implementation::CarrotClass<Sender,Receiver,T,U,std::tuple<std::decay_t<SignalArgs>...>,std::tuple<std::decay_t<SlotArgs>...>>
    connect(Sender *sender, Event<SignalArgs...> T::* event, Receiver *receiver, void(U::* handler)(SlotArgs...), SIGSLOT type = SIGSLOT::AutoConnection);

    template<typename Sender, typename Receiver, typename T, typename U, typename ...SignalArgs, typename ...SlotArgs>
    static constexpr Implementation::CarrotClass<Sender,Receiver,T,U,std::tuple<std::decay_t<SignalArgs>...>,std::tuple<std::decay_t<SlotArgs>...>>
    disconnect(Sender *sender, Event<SignalArgs...> T::* event, Receiver *receiver, void(U::* handler)(SlotArgs...));

    template<typename Sender, typename Receiver, typename T, typename U, typename ...SignalArgs, typename ...SlotArgs>
    static constexpr Implementation::CarrotClass<Sender,Receiver,T,U,std::tuple<std::decay_t<SignalArgs>...>,std::tuple<std::decay_t<SlotArgs>...>>
    connect(Sender *sender, Event<SignalArgs...> T::* event, Receiver *receiver, void(U::* handler)(SlotArgs...) const, SIGSLOT type = SIGSLOT::AutoConnection);

    template<typename Sender, typename Receiver, typename T, typename U, typename ...SignalArgs, typename ...SlotArgs>
    static constexpr Implementation::CarrotClass<Sender,Receiver,T,U,std::tuple<std::decay_t<SignalArgs>...>,std::tuple<std::decay_t<SlotArgs>...>>
    disconnect(Sender *sender, Event<SignalArgs...> T::* event, Receiver *receiver, void(U::* handler)(SlotArgs...) const);

    //connect lambda
    //连接Lambda
    template<typename Sender, typename Receiver, typename T, typename Lambda, typename ...SignalArgs>
    static constexpr Implementation::CarrotLambda<Sender,Receiver,T,Lambda,std::tuple<std::decay_t<SignalArgs>...>>
    connect(Sender *sender, Event<SignalArgs...> T::* event, Receiver *receiver, Lambda &&lambda, SIGSLOT type = SIGSLOT::AutoConnection);

    //connect global function and static function
    //连接全局函数，静态函数
    template<typename Sender, typename T, typename ...SignalArgs, typename ...SlotArgs>
    static constexpr Implementation::GloablCarrot<Sender,T,std::tuple<std::decay_t<SignalArgs>...>,std::tuple<std::decay_t<SlotArgs>...>>
    connect(Sender *sender, Event<SignalArgs...> T::* event, void(*handler)(SlotArgs...));

    template<typename Sender, typename T, typename ...SignalArgs, typename ...SlotArgs>
    static constexpr Implementation::GloablCarrot<Sender,T,std::tuple<std::decay_t<SignalArgs>...>,std::tuple<std::decay_t<SlotArgs>...>>
    disconnect(Sender *sender, Event<SignalArgs...> T::* event, void(*handler)(SlotArgs...));
}

namespace SigSlot::Implementation
{
    //function type erasure related
    //函数类型擦除相关
    template<typename ...Args>
    class EventHandlerInterface
    {
    public:
        EventHandlerInterface(const EventHandlerInterface &) = delete;
        EventHandlerInterface &operator=(const EventHandlerInterface &event) = delete;
        EventHandlerInterface() = default;
        virtual ~EventHandlerInterface() = default;

        //overload () is for convenience of calling later
        //重载()是为了方便之后调用
        virtual void operator()(const Args &...args) = 0;
    };

    //provide two specializations, one that does not need to store the this pointer
    //The other class member function needs an object pointer, so the class object pointer is also stored
    //提供两种特化,不用存储this指针的一类
    //另一类如类成员函数是需要对象指针的,所以把类对象指针也存起来

    //main template
    //主模板
    template<typename T, typename Tuple, typename ...Args>
    class EventHandler final : public EventHandlerInterface<Args...> {};

    template<typename ...SlotArgs, typename ...Args>
    class EventHandler<void, std::tuple<SlotArgs...>, Args...> final : public EventHandlerInterface<Args...>
    {
    private:
        std::function<void(SlotArgs...)> m_Handler;
    public:
        EventHandler(const EventHandler &eventHandler) = delete;
        EventHandler &operator=(const EventHandler &eventHandler) = delete;
        EventHandler(EventHandler &&eventHandler) = delete;
        EventHandler &operator=(EventHandler &&eventHandler) = delete;

        template<typename Callable>
        explicit EventHandler(Callable &&func) noexcept
        {
            m_Handler = std::forward<Callable>(func);
        }

        void operator()(const Args &... args) final
        {
            Apply(m_Handler, std::make_tuple(args...));
        }
    };


    template<typename T, typename ...SlotArgs, typename ...Args>
    class EventHandler<T, std::tuple<SlotArgs...>, Args...> final : public EventHandlerInterface<Args...>
    {
        using FunctionPointer = void (T::*)(SlotArgs...);
        using ConstFunctionPointer = void (T::*)(SlotArgs...) const;
    private:
        T *m_Receiver;
        std::function<void(T *, SlotArgs...)> m_Handler;
    public:
        EventHandler(const EventHandler &eventHandler) = delete;
        EventHandler &operator=(const EventHandler &eventHandler) = delete;
        EventHandler(EventHandler &&eventHandler) = delete;
        EventHandler &operator=(EventHandler &&eventHandler) = delete;

        EventHandler(T *receiver, FunctionPointer handler) noexcept
        {
            m_Receiver = receiver;
            m_Handler = handler;
        }

        EventHandler(T *receiver, ConstFunctionPointer handler) noexcept
        {
            m_Receiver = receiver;
            m_Handler = handler;
        }

        void operator()(const Args &...args) final
        {
            Apply(m_Handler, std::make_tuple(m_Receiver, args...));
        }
    };

    //solve the problem of different compiler function pointer sizes, different compiler class member function pointer sizes are different
    //处理不同编译器指针大小差异,不同编译器类成员函数指针大小不一样
    //msvc is 8 bytes, mingw is 16 bytes
    template<bool Is64, std::size_t size>
    struct ClassFunctionPointer;

    template<>
    struct ClassFunctionPointer<true, 8>
    {
        void *address = nullptr;
        bool operator==(const ClassFunctionPointer &other) const
        {
            return address == other.address;
        }

        bool operator<(const ClassFunctionPointer &other) const
        {
            return address < other.address;
        }
    };

    template<>
    struct ClassFunctionPointer<true, 16>
    {
        void *address[2] = {nullptr, nullptr};
        bool operator==(const ClassFunctionPointer &other) const
        {
            return address[0] == other.address[0] && address[1] == other.address[1];
        }

        bool operator<(const ClassFunctionPointer &other) const
        {
            return address[0] < other.address[0] || (address[0] == other.address[0] && address[1] < other.address[1]);
        }
    };

    template<>
    struct ClassFunctionPointer<false, 4>
    {
        void *address = nullptr;
        bool operator==(const ClassFunctionPointer &other) const
        {
            return address == other.address;
        }

        bool operator<(const ClassFunctionPointer &other) const
        {
            return address < other.address;
        }
    };

    template<>
    struct ClassFunctionPointer<false, 8>
    {
        void *address[2] = {nullptr, nullptr};
        bool operator==(const ClassFunctionPointer &other) const
        {
            return address[0] == other.address[0] && address[1] == other.address[1];
        }

        bool operator<(const ClassFunctionPointer &other) const
        {
            return address[1] < other.address[1] || (address[1] == other.address[1] && address[0] < other.address[0]);
        }
    };

    using CFP = ClassFunctionPointer<(sizeof(void *) == 8), sizeof(void (Object::*)())>;
    //store function address related, object stores object pointer, function stores function pointer
    //存储函数地址相关,object存放对象指针，function存放函数指针
    struct Address
    {
        void *object;
        CFP function;

        template<typename T, typename U, typename ...Args>
        constexpr Address(T *receiver, void(U::* handler)(Args...)) noexcept
        {
            object = receiver;
            memcpy(&function, &handler, sizeof(CFP));
        }

        template<typename T, typename U,typename ...Args>
        constexpr Address(T *receiver, void(U::* handler)(Args...) const) noexcept
        {
            object = receiver;
            memcpy(&function, &handler, sizeof(CFP));
        }

        template<typename T,typename U, typename ...Args>
        constexpr Address(T *sender, Event<Args...> U::* event) noexcept
        {
            object = sender;
            memcpy(&function, &event, sizeof(event));
        }

        template<typename ...Args>
        constexpr explicit Address(void(*handler)(Args...)) noexcept
        {
            object = nullptr;
            memcpy(&function, &handler, sizeof(void (*)()));
        }

        bool operator==(const Address &other) const noexcept
        {
            return other.object == object && other.function == function;
        }

    };

    //hash 函数计算hash值
    struct AddressHash
    {
        std::size_t operator()(const Address &address) const
        {
            std::size_t h1 = std::hash<void *>{}(address.object);
            constexpr static bool condition = (sizeof(void (Address::*)()) == sizeof(void *) || sizeof(void (Address::*)()) == sizeof(void *) * 2);
            static_assert(condition, "SigSlot can't work ");
            //根据不同编译器指针大小计算hash值
            if constexpr (sizeof(address.function) == sizeof(void *))
            {
                std::size_t h2 = std::hash<void *>{}((void *) address.function.address);
                return h1 ^ (h2 << 1);
            }
            else if constexpr (sizeof(address.function) == sizeof(void *) * 2)
            {
                void *functionAddress[2] = {nullptr, nullptr};
                memcpy(functionAddress, &address.function, sizeof(void *) * 2);
                std::size_t h2 = std::hash<void *>{}(functionAddress[0]);
                std::size_t h3 = std::hash<void *>{}(functionAddress[1]);
                return h1 ^ (h2 << 1) ^ (h3 << 2);
            }
        }
    };

    template<typename Callable>
    static void AddSender(Object &object, const Address &senderAddress, const CFP &functionAddress, Callable &&func);

    template<typename Callable>
    static void AddReceiver(Object &object, const Address &receiverAddress, Callable &&func);

    static void RemoveSender(Object &object, const Address &senderAddress, const CFP &functionAddress);

    static void RemoveReceiver(Object &object, const Address &address);

    static bool ContainSender(const Object &object, const Address &address);

    static bool ContainReceiver(const Object &object, const Address &address);

    //event loop manager
    // 事件循环的管理相关
    class EventLoopManager
    {
    private:
        std::unordered_map<std::thread::id, EventLoop *> m_EventLoops;
        std::mutex m_Mutex;
        EventLoopManager() = default;
        ~EventLoopManager() = default;

    public:
        EventLoopManager(const EventLoopManager &) = delete;
        EventLoopManager &operator=(const EventLoopManager &) = delete;

        static EventLoopManager *GetInstance() noexcept
        {
            static EventLoopManager instance;
            return &instance;
        }

        void AddEventLoop(EventLoop *loop)
        {
            std::unique_lock<std::mutex> lock(m_Mutex);
            m_EventLoops.insert(std::make_pair(std::this_thread::get_id(), loop));
        }

        void RemoveEventLoop()
        {
            std::unique_lock<std::mutex> lock(m_Mutex);
            if (m_EventLoops.count(std::this_thread::get_id()))
            {
                m_EventLoops.erase(std::this_thread::get_id());
            }
        }

        EventLoop *GetEventLoop(std::thread::id id)
        {
            std::unique_lock<std::mutex> lock(m_Mutex);
            if (m_EventLoops.count(id))
            {
                return m_EventLoops[id];
            }
            return nullptr;
        }
    };
}
namespace SigSlot
{
    class EventLoop
    {
    private:
        std::mutex m_Mutex;
        std::condition_variable m_Condition;
        std::deque<std::function<void()>> m_Messages;
        std::atomic<bool> m_Quit{};
    public:
        EventLoop()
        {
            Implementation::EventLoopManager::GetInstance()->AddEventLoop(this);
            m_Quit = false;
        }

        ~EventLoop()
        {
            Implementation::EventLoopManager::GetInstance()->RemoveEventLoop();
            Quit();
        }

        template<typename Callable>
        void PostEvent(Callable &&func)
        {
            std::unique_lock<std::mutex> lock(m_Mutex);
            m_Messages.push_back(std::forward<Callable>(func));
            m_Condition.notify_all();
        }

        template<typename Callable>
        void SendEvent(Callable &&func)
        {
            std::unique_lock<std::mutex> lock(m_Mutex);
            std::atomic<bool> Finished = false;
            m_Messages.push_back([func = std::forward<Callable>(func), &Finished]()
            {
                func();
                Finished = true;
            });
            m_Condition.notify_all();
            m_Condition.wait(lock, [&Finished]
            {
                return Finished.load();
            });
        }

        void Update()
        {
            std::unique_lock<std::mutex> lock(m_Mutex);
            m_Condition.wait(lock, [this]
            {
                return !m_Messages.empty() || m_Quit;
            });
            std::function<void()> func = std::move(m_Messages.front());
            m_Messages.pop_front();
            lock.unlock();
            func();
            m_Condition.notify_all();
        }

        void Run()
        {
            m_Quit = false;
            while (!m_Quit)
            {
                Update();
            }
        }

        void Quit()
        {
            m_Quit = true;
            m_Condition.notify_all();
        }
    };

    class Thread
    {
    private:
        std::thread::id id;
        std::thread thread;
    public:
        Thread(const Thread &other) = delete;

        Thread &operator=(const Thread &other) = delete;

        Thread(Thread &&other) noexcept
        {
            id = other.id;
            thread = std::move(other.thread);
        }

        Thread &operator=(Thread &&other) noexcept
        {
            id = other.id;
            thread = std::move(other.thread);
            return *this;
        }

        template<typename Callable, typename ...Args>
        explicit Thread(Callable &&func, Args &&... args) :thread([&]()
        {
            id = std::this_thread::get_id();
            std::forward<Callable>(func)(std::forward<Args>(args)...);
        })
        {
            while (GetEventLoop(id) == nullptr)
            {
                std::this_thread::yield();
            }
            thread.detach();
        }

        std::thread::id GetId() const
        {
            return id;
        }
    };

    template<typename ...Args>
    class Event
    {
    private:
        using Address = Implementation::Address;
        using AddressHash = Implementation::AddressHash;
        struct Handler
        {
            std::thread::id id;
            std::shared_ptr<Implementation::EventHandlerInterface<Args...>> handler;
            SIGSLOT type;
        };
    private:
        std::unordered_map<Address, Handler, AddressHash> m_Handlers;
        mutable std::shared_mutex m_Mutex;
    private:
        void AddHandler(const Address &address, const Handler &handler)
        {
            std::unique_lock<std::shared_mutex> lock(m_Mutex);
            if (!m_Handlers.count(address))
            {
                m_Handlers.insert(std::make_pair(address, handler));
            }
        }

        void RemoveHandler(const Address &address)
        {
            std::unique_lock<std::shared_mutex> lock(m_Mutex);
            if (m_Handlers.count(address))
            {
                m_Handlers.erase(address);
            }
        }

        //处理仿函数的接口
        template<typename T, typename U, typename ...SlotArgs>
        Address ImitationFunctionHelper(T *object, U &&t, void(std::decay_t<U>::* func)(SlotArgs...), SIGSLOT type)
        {
            constexpr bool is_object_v = Implementation::is_object<T, std::thread::id>::value;
            Address address = Address(object, func);
            Handler v_handler;
            v_handler.id = std::this_thread::get_id();
            v_handler.handler = std::make_shared<Implementation::EventHandler<void, std::tuple<SlotArgs...>, Args...>>(std::forward<U>(t));
            v_handler.type = type;
            if constexpr (is_object_v)
            {
                v_handler.id = object->ThreadId();
            }
            AddHandler(address, v_handler);
            return address;
        }

        template<typename T, typename U, typename ...SlotArgs>
        Address ImitationFunctionHelper(T *object, U &&t, void(std::decay_t<U>::* func)(SlotArgs...) const, SIGSLOT type)
        {
            constexpr bool is_object_v = Implementation::is_object<T, std::thread::id>::value;
            Address address = Address(object, func);
            Handler v_handler;
            v_handler.id = std::this_thread::get_id();
            v_handler.handler = std::make_shared<Implementation::EventHandler<void, std::tuple<SlotArgs...>, Args...>>(std::forward<U>(t));
            v_handler.type = type;
            if constexpr (is_object_v)
            {
                v_handler.id = object->ThreadId();
            }
            AddHandler(address, v_handler);
            return address;
        }

    public:
        ~Event()
        {
            m_Handlers.clear();
        }

        void emit(const Args &... args)
        {
            std::shared_lock<std::shared_mutex> lock(m_Mutex);
            for (auto &&element: m_Handlers)
            {
                switch (element.second.type)
                {
                    case SIGSLOT::AutoConnection:
                    {
                        if (element.second.id == std::this_thread::get_id())
                        {
                            (*element.second.handler)(args...);
                        }
                        else
                        {
                            EventLoop *loop = Implementation::EventLoopManager::GetInstance()->GetEventLoop(element.second.id);
                            if (loop != nullptr)
                            {
                                loop->PostEvent([handler = element.second.handler, args...]
                                {
                                    (*handler)(args...);
                                });
                            }
                        }
                        break;
                    }
                    case SIGSLOT::DirectConnection:
                    {
                        (*element.second.handler)(args...);
                        break;
                    }
                    case SIGSLOT::QueuedConnection:
                    {
                        EventLoop *loop = Implementation::EventLoopManager::GetInstance()->GetEventLoop(element.second.id);
                        if (loop != nullptr)
                        {
                            loop->PostEvent([handler = element.second.handler, args...]
                            {
                                (*handler)(args...);
                            });
                        }
                        break;
                    }
                    case SIGSLOT::BlockingQueuedConnection:
                    {
                        EventLoop *loop = Implementation::EventLoopManager::GetInstance()->GetEventLoop(element.second.id);
                        if (loop != nullptr)
                        {
                            loop->SendEvent([handler = element.second.handler, args...]
                            {
                                (*handler)(args...);
                            });
                        }
                        break;
                    }
                }
            }
        }

    public:
        //connect member function
        //连接普通成员函数
        template<typename Sender, typename Receiver, typename T, typename U, typename ...SignalArgs, typename ...SlotArgs>
        friend constexpr Implementation::CarrotClass<Sender, Receiver, T, U, std::tuple<std::decay_t<SignalArgs>...>, std::tuple<std::decay_t<SlotArgs>...>>
        connect(Sender *sender, Event<SignalArgs...> T::* event, Receiver *receiver, void(U::* handler)(SlotArgs...), SIGSLOT type);

        template<typename Sender, typename Receiver, typename T, typename U, typename ...SignalArgs, typename ...SlotArgs>
        friend constexpr Implementation::CarrotClass<Sender, Receiver, T, U, std::tuple<std::decay_t<SignalArgs>...>, std::tuple<std::decay_t<SlotArgs>...>>
        disconnect(Sender *sender, Event<SignalArgs...> T::* event, Receiver *receiver, void(U::* handler)(SlotArgs...));

        template<typename Sender, typename Receiver, typename T, typename U, typename ...SignalArgs, typename ...SlotArgs>
        friend constexpr Implementation::CarrotClass<Sender, Receiver, T, U, std::tuple<std::decay_t<SignalArgs>...>, std::tuple<std::decay_t<SlotArgs>...>>
        connect(Sender *sender, Event<SignalArgs...> T::* event, Receiver *receiver, void(U::* handler)(SlotArgs...) const, SIGSLOT type);

        template<typename Sender, typename Receiver, typename T, typename U, typename ...SignalArgs, typename ...SlotArgs>
        friend constexpr Implementation::CarrotClass<Sender, Receiver, T, U, std::tuple<std::decay_t<SignalArgs>...>, std::tuple<std::decay_t<SlotArgs>...>>
        disconnect(Sender *sender, Event<SignalArgs...> T::* event, Receiver *receiver, void(U::* handler)(SlotArgs...) const);

        //connect lambda
        //连接Lambda
        template<typename Sender, typename Receiver, typename T, typename Lambda, typename ...SignalArgs>
        friend constexpr Implementation::CarrotLambda<Sender, Receiver, T, Lambda, std::tuple<std::decay_t<SignalArgs>...>>
        connect(Sender *sender, Event<SignalArgs...> T::* event, Receiver *receiver, Lambda &&lambda, SIGSLOT type);

        //connect global function and static function
        //连接全局函数，静态函数
        template<typename Sender, typename T, typename ...SignalArgs, typename ...SlotArgs>
        friend constexpr Implementation::GloablCarrot<Sender, T, std::tuple<std::decay_t<SignalArgs>...>, std::tuple<std::decay_t<SlotArgs>...>>
        connect(Sender *sender, Event<SignalArgs...> T::* event, void(*handler)(SlotArgs...));

        template<typename Sender, typename T, typename ...SignalArgs, typename ...SlotArgs>
        friend constexpr Implementation::GloablCarrot<Sender, T, std::tuple<std::decay_t<SignalArgs>...>, std::tuple<std::decay_t<SlotArgs>...>>
        disconnect(Sender *sender, Event<SignalArgs...> T::* event, void(*handler)(SlotArgs...));

    };


    class Object
    {
    private:
        using Address = Implementation::Address;
        using AddressHash = Implementation::AddressHash;
        using Connection = std::map<Implementation::CFP, std::function<void()>>;
    private:
        //注意一个订阅者可以使用不同的函数取订阅这个对象，但是存的都是同一个事件，所以用vector存起来
        std::atomic<std::thread::id> m_Id;
        std::unordered_map<Address, Connection, AddressHash> m_SendersList;
        std::unordered_map<Address, std::function<void()>, AddressHash> m_ReceiversList;
        mutable std::shared_mutex m_Mutex;
    private:
        template<typename Callable>
        void AddSender(const Address &senderAddress, const Implementation::CFP &functionAddress, Callable &&func)
        {
            std::unique_lock<std::shared_mutex> lock(m_Mutex);
            m_SendersList[senderAddress].insert(std::make_pair(functionAddress, std::forward<Callable>(func)));
        }

        template<typename Callable>
        void AddReceiver(const Address &receiverAddress, Callable &&func)
        {
            std::unique_lock<std::shared_mutex> lock(m_Mutex);
            m_ReceiversList[receiverAddress] = std::forward<Callable>(func);
        }

        void RemoveSender(const Address &senderAddress, const Implementation::CFP &functionAddress)
        {
            std::unique_lock<std::shared_mutex> lock(m_Mutex);
            m_SendersList.at(senderAddress).erase(functionAddress);
        }

        void RemoveReceiver(const Address &address)
        {
            std::unique_lock<std::shared_mutex> lock(m_Mutex);
            m_ReceiversList.erase(address);
        }

        bool ContainSender(const Address &address) const
        {
            std::shared_lock<std::shared_mutex> lock(m_Mutex);
            return m_SendersList.count(address);
        }

        bool ContainReceiver(const Address &address) const
        {
            std::shared_lock<std::shared_mutex> lock(m_Mutex);
            return m_ReceiversList.count(address);
        }

    public:
        template<typename Callable>
        friend void Implementation::AddSender(Object &object, const Address &senderAddress, const Implementation::CFP &functionAddress, Callable &&func);
        template<typename Callable>
        friend void Implementation::AddReceiver(Object &object, const Address &receiverAddress, Callable &&func);
        friend void Implementation::RemoveSender(Object &object, const Address &senderAddress, const Implementation::CFP &functionAddress);
        friend void Implementation::RemoveReceiver(Object &object, const Address &address);
        friend bool Implementation::ContainSender(const Object &object, const Address &address);
        friend bool Implementation::ContainReceiver(const Object &object, const Address &address);

        Object()
        {
            m_Id = std::this_thread::get_id();
        }

        virtual ~Object()
        {
            for (auto &pair: m_SendersList)
            {
                for (auto &func: pair.second)
                {
                    func.second();
                }
            }
            for (auto &receiver: m_ReceiversList)
            {
                (receiver.second)();
            }
        }

        void MoveToThread(const std::thread &target) noexcept
        {
            m_Id = target.get_id();
        }

        void MoveToThread(const std::thread::id &id) noexcept
        {
            m_Id = id;
        }

        void MoveToThread(const Thread &other) noexcept
        {
            m_Id = other.GetId();
        }

        std::thread::id ThreadId() const noexcept
        {
            return m_Id;
        }

    public:
        //connect member function
        //连接普通成员函数
        template<typename Sender, typename Receiver, typename T, typename U, typename ...SignalArgs, typename ...SlotArgs>
        friend constexpr Implementation::CarrotClass<Sender, Receiver, T, U, std::tuple<std::decay_t<SignalArgs>...>, std::tuple<std::decay_t<SlotArgs>...>>
        connect(Sender *sender, Event<SignalArgs...> T::* event, Receiver *receiver, void(U::* handler)(SlotArgs...), SIGSLOT type);

        template<typename Sender, typename Receiver, typename T, typename U, typename ...SignalArgs, typename ...SlotArgs>
        friend constexpr Implementation::CarrotClass<Sender, Receiver, T, U, std::tuple<std::decay_t<SignalArgs>...>, std::tuple<std::decay_t<SlotArgs>...>>
        disconnect(Sender *sender, Event<SignalArgs...> T::* event, Receiver *receiver, void(U::* handler)(SlotArgs...));

        template<typename Sender, typename Receiver, typename T, typename U, typename ...SignalArgs, typename ...SlotArgs>
        friend constexpr Implementation::CarrotClass<Sender, Receiver, T, U, std::tuple<std::decay_t<SignalArgs>...>, std::tuple<std::decay_t<SlotArgs>...>>
        connect(Sender *sender, Event<SignalArgs...> T::* event, Receiver *receiver, void(U::* handler)(SlotArgs...) const, SIGSLOT type);

        template<typename Sender, typename Receiver, typename T, typename U, typename ...SignalArgs, typename ...SlotArgs>
        friend constexpr Implementation::CarrotClass<Sender, Receiver, T, U, std::tuple<std::decay_t<SignalArgs>...>, std::tuple<std::decay_t<SlotArgs>...>>
        disconnect(Sender *sender, Event<SignalArgs...> T::* event, Receiver *receiver, void(U::* handler)(SlotArgs...) const);

        //connect lambda
        //连接Lambda
        template<typename Sender, typename Receiver, typename T, typename Lambda, typename ...SignalArgs>
        friend constexpr Implementation::CarrotLambda<Sender, Receiver, T, Lambda, std::tuple<std::decay_t<SignalArgs>...>>
        connect(Sender *sender, Event<SignalArgs...> T::* event, Receiver *receiver, Lambda &&lambda, SIGSLOT type);

        //connect global function and static function
        //连接全局函数，静态函数
        template<typename Sender, typename T, typename ...SignalArgs, typename ...SlotArgs>
        friend constexpr Implementation::GloablCarrot<Sender, T, std::tuple<std::decay_t<SignalArgs>...>, std::tuple<std::decay_t<SlotArgs>...>>
        connect(Sender *sender, Event<SignalArgs...> T::* event, void(*handler)(SlotArgs...));

        template<typename Sender, typename T, typename ...SignalArgs, typename ...SlotArgs>
        friend constexpr Implementation::GloablCarrot<Sender, T, std::tuple<std::decay_t<SignalArgs>...>, std::tuple<std::decay_t<SlotArgs>...>>
        disconnect(Sender *sender, Event<SignalArgs...> T::* event, void(*handler)(SlotArgs...));

    };
}
namespace SigSlot::Implementation
{
    template<typename Callable>
    inline void AddSender(Object &object, const Address &senderAddress, const Implementation::CFP &functionAddress, Callable &&func)
    {
        object.AddSender(senderAddress, functionAddress, std::forward<Callable>(func));
    }

    template<typename Callable>
    inline void AddReceiver(Object &object, const Address &receiverAddress, Callable &&func)
    {
        object.AddReceiver(receiverAddress, std::forward<Callable>(func));
    }

    inline void RemoveSender(Object &object, const Address &senderAddress, const Implementation::CFP &functionAddress)
    {
        object.RemoveSender(senderAddress, functionAddress);
    }

    inline void RemoveReceiver(Object &object, const Address &address)
    {
        object.RemoveReceiver(address);
    }

    inline bool ContainSender(const Object &object, const Address &address)
    {
        return object.ContainSender(address);
    }

    inline bool ContainReceiver(const Object &object, const Address &address)
    {
        return object.ContainReceiver(address);
    }

}
namespace SigSlot
{
    template<typename Sender, typename Receiver, typename T, typename U, typename ...SignalArgs, typename ...SlotArgs>
    static constexpr Implementation::CarrotClass<Sender,Receiver,T,U,std::tuple<std::decay_t<SignalArgs>...>,std::tuple<std::decay_t<SlotArgs>...>>
    connect(Sender *sender, Event<SignalArgs...> T::* event, Receiver *receiver, void (U::* handler)(SlotArgs...), SIGSLOT type)
    {
        Implementation::Address ReceiverAddress(receiver, handler);
        Implementation::Address SenderAddress(sender, event);

        using Handler = typename Event<SignalArgs...>::Handler;
        Handler v_handler;
        v_handler.handler = std::make_shared<Implementation::EventHandler<U, std::tuple<SlotArgs...>, SignalArgs...>>((U *) receiver, handler);
        v_handler.type = type;
        v_handler.id = std::this_thread::get_id();

        constexpr bool is_object_v = Implementation::is_object<T, std::thread::id>::value && Implementation::is_object<U, std::thread::id>::value;
        constexpr bool is_not_object_v = !Implementation::is_object<T, std::thread::id>::value && !Implementation::is_object<U, std::thread::id>::value;
        static_assert(is_object_v || is_not_object_v, "Sender and Receiver must both be Object or neither be");
        if constexpr (is_object_v)
        {
            v_handler.id = receiver->ThreadId();
            sender->AddReceiver(ReceiverAddress, [=]()
            {
                receiver->RemoveSender(SenderAddress, ReceiverAddress.function);
            });
            receiver->AddSender(SenderAddress, ReceiverAddress.function, [=]()
            {
                (static_cast<T *>(sender)->*event).RemoveHandler(ReceiverAddress);
                sender->RemoveReceiver(ReceiverAddress);
            });
        }
        (static_cast<T *>(sender)->*event).AddHandler(ReceiverAddress, v_handler);
    }

    template<typename Sender, typename Receiver, typename T, typename U, typename ...SignalArgs, typename ...SlotArgs>
    static constexpr Implementation::CarrotClass<Sender,Receiver,T,U,std::tuple<std::decay_t<SignalArgs>...>,std::tuple<std::decay_t<SlotArgs>...>>
    disconnect(Sender *sender, Event<SignalArgs...> T::* event, Receiver *receiver, void(U::* handler)(SlotArgs...))
    {
        Implementation::Address ReceiverAddress(receiver, handler);
        Implementation::Address SenderAddress(sender, event);
        (static_cast<T *>(sender)->*event).RemoveHandler(ReceiverAddress);

        constexpr bool is_object_v = Implementation::is_object<T, std::thread::id>::value && Implementation::is_object<U, std::thread::id>::value;
        constexpr bool is_not_object_v = !Implementation::is_object<T, std::thread::id>::value && !Implementation::is_object<U, std::thread::id>::value;
        static_assert(is_object_v || is_not_object_v, "Sender and Receiver must both be Object or neither be");
        if constexpr (is_object_v)
        {
            if (sender->ContainReceiver(ReceiverAddress) && receiver->ContainSender(SenderAddress))
            {
                sender->RemoveReceiver(ReceiverAddress);
                receiver->RemoveSender(SenderAddress, ReceiverAddress.function);
            }
        }
    }

    template<typename Sender, typename Receiver, typename T, typename U, typename ...SignalArgs, typename ...SlotArgs>
    static constexpr Implementation::CarrotClass<Sender,Receiver,T,U,std::tuple<std::decay_t<SignalArgs>...>,std::tuple<std::decay_t<SlotArgs>...>>
    connect(Sender *sender, Event<SignalArgs...> T::* event, Receiver *receiver, void (U::* handler)(SlotArgs...) const, SIGSLOT type)
    {
        Implementation::Address ReceiverAddress(receiver, handler);
        Implementation::Address SenderAddress(sender, event);

        using Handler = typename Event<SignalArgs...>::Handler;
        Handler v_handler;
        v_handler.handler = std::make_shared<Implementation::EventHandler<U, std::tuple<SlotArgs...>, SignalArgs...>>((U *) receiver, handler);
        v_handler.type = type;
        v_handler.id = std::this_thread::get_id();

        constexpr bool is_object_v = Implementation::is_object<T, std::thread::id>::value && Implementation::is_object<U, std::thread::id>::value;
        constexpr bool is_not_object_v = !Implementation::is_object<T, std::thread::id>::value && !Implementation::is_object<U, std::thread::id>::value;
        static_assert(is_object_v || is_not_object_v, "Sender and Receiver must both be Object or neither be");
        if constexpr (is_object_v)
        {
            v_handler.id = receiver->ThreadId();
            sender->AddReceiver(ReceiverAddress, [=]()
            {
                receiver->RemoveSender(SenderAddress, ReceiverAddress.function);
            });
            receiver->AddSender(SenderAddress, ReceiverAddress.function, [=]()
            {
                (static_cast<T *>(sender)->*event).RemoveHandler(ReceiverAddress);
                sender->RemoveReceiver(ReceiverAddress);
            });
        }

        (static_cast<T *>(sender)->*event).AddHandler(ReceiverAddress, v_handler);
    }

    template<typename Sender, typename Receiver, typename T, typename U, typename ...SignalArgs, typename ...SlotArgs>
    static constexpr Implementation::CarrotClass<Sender,Receiver,T,U,std::tuple<std::decay_t<SignalArgs>...>,std::tuple<std::decay_t<SlotArgs>...>>
    disconnect(Sender *sender, Event<SignalArgs...> T::* event, Receiver *receiver, void(U::* handler)(SlotArgs...) const)
    {
        Implementation::Address ReceiverAddress(receiver, handler);
        Implementation::Address SenderAddress(sender, event);
        (static_cast<T *>(sender)->*event).RemoveHandler(ReceiverAddress);

        constexpr bool is_object_v = Implementation::is_object<T, std::thread::id>::value && Implementation::is_object<U, std::thread::id>::value;
        constexpr bool is_not_object_v = !Implementation::is_object<T, std::thread::id>::value && !Implementation::is_object<U, std::thread::id>::value;
        static_assert(is_object_v || is_not_object_v, "Sender and Receiver must both be Object or neither be");
        if constexpr (is_object_v)
        {
            if (sender->ContainReceiver(ReceiverAddress) && receiver->ContainSender(SenderAddress))
            {
                sender->RemoveReceiver(ReceiverAddress);
                receiver->RemoveSender(SenderAddress, ReceiverAddress.function);
            }
        }
    }

    template<typename Sender, typename Receiver, typename T, typename Lambda, typename ...SignalArgs>
    inline constexpr Implementation::CarrotLambda<Sender,Receiver,T,Lambda,std::tuple<std::decay_t<SignalArgs>...>>
    connect(Sender *sender, Event<SignalArgs...> T::* event, Receiver *receiver, Lambda &&lambda, SIGSLOT type)
    {
        constexpr bool is_object_v = Implementation::is_object<T, std::thread::id>::value && Implementation::is_object<Receiver, std::thread::id>::value;
        constexpr bool is_not_object_v = !Implementation::is_object<T, std::thread::id>::value && !Implementation::is_object<Receiver, std::thread::id>::value;
        static_assert(is_object_v || is_not_object_v, "Sender and Receiver must both be Object or neither be");
        Implementation::Address SenderAddress(sender, event);
        //借用之前的lambda辅助函数来进行lambda的提取
        auto &&ReceiverAddress = (sender->*event).ImitationFunctionHelper(receiver, lambda, &Lambda::operator(), type);
        if constexpr (is_object_v)
        {
            sender->AddReceiver(ReceiverAddress, [=]()
            {
                receiver->RemoveSender(SenderAddress, ReceiverAddress.function);
            });
            receiver->AddSender(SenderAddress, ReceiverAddress.function, [=]()
            {
                (static_cast<T *>(sender)->*event).RemoveHandler(ReceiverAddress);
                sender->RemoveReceiver(ReceiverAddress);
            });
        }
    }

    template<typename Sender, typename T, typename ...SignalArgs, typename ...SlotArgs>
    inline constexpr Implementation::GloablCarrot<Sender,T,std::tuple<std::decay_t<SignalArgs>...>,std::tuple<std::decay_t<SlotArgs>...>>
    connect(Sender *sender, Event<SignalArgs...> T::* event, void(*handler)(SlotArgs...))
    {
        using Handler = typename Event<SignalArgs...>::Handler;
        Handler v_handler
        {
            std::this_thread::get_id(),
            std::make_shared<Implementation::EventHandler<void, std::tuple<SlotArgs...>, SignalArgs...>>(handler),
            SIGSLOT::DirectConnection,
        };
        (static_cast<T *>(sender)->*event).AddHandler(Implementation::Address(handler), v_handler);
    }

    template<typename Sender, typename T, typename ...SignalArgs, typename ...SlotArgs>
    inline constexpr Implementation::GloablCarrot<Sender,T,std::tuple<std::decay_t<SignalArgs>...>,std::tuple<std::decay_t<SlotArgs>...>>
    disconnect(Sender *sender, Event<SignalArgs...> T::* event, void(*handler)(SlotArgs...))
    {
        (static_cast<T *>(sender)->*event).RemoveHandler(Implementation::Address(handler));
    }

    inline EventLoop *GetEventLoop(std::thread::id id)
    {
        return Implementation::EventLoopManager::GetInstance()->GetEventLoop(id);
    }

    inline EventLoop *GetEventLoop()
    {
        return GetEventLoop(std::this_thread::get_id());
    }

    inline EventLoop *GetEventLoop(const Thread &thread)
    {
        return GetEventLoop(thread.GetId());
    }

    inline EventLoop *GetEventLoop(const std::thread &thread)
    {
        return GetEventLoop(thread.get_id());
    }
}
#define SIGSLOT_OBJECT \
    private:\
    using SIGSLOT_FunctionPointer = SigSlot::Implementation::ClassFunctionPointer<sizeof(void *) == 8, sizeof(void (SigSlot::Implementation::Address::*)(int))>;\
    SigSlot::Object SIGSLOT__Object;\
    public:\
        template<typename Callable>\
        void AddSender(const SigSlot::Implementation::Address &SIGSLOT_senderAddress, const SIGSLOT_FunctionPointer &SIGSLOT_functionAddress, Callable &&SIGSLOT_func)\
        {\
            SigSlot::Implementation::AddSender(SIGSLOT__Object,SIGSLOT_senderAddress,SIGSLOT_functionAddress,std::forward<Callable>(SIGSLOT_func));\
        }\
\
        template<typename Callable>\
        void AddReceiver(const SigSlot::Implementation::Address &SIGSLOT_receiverAddress, Callable &&SIGSLOT_func)\
        {\
            SigSlot::Implementation::AddReceiver(SIGSLOT__Object,SIGSLOT_receiverAddress,std::forward<Callable>(SIGSLOT_func));\
        }\
\
        void RemoveSender(const SigSlot::Implementation::Address &SIGSLOT_senderAddress, const SIGSLOT_FunctionPointer &SIGSLOT_functionAddress)\
        {\
            SigSlot::Implementation::RemoveSender(SIGSLOT__Object,SIGSLOT_senderAddress,SIGSLOT_functionAddress);\
        }\
\
        void RemoveReceiver(const SigSlot::Implementation::Address &SIGSLOT_address)\
        {\
            SigSlot::Implementation::RemoveReceiver(SIGSLOT__Object,SIGSLOT_address);\
        }\
\
        bool ContainSender(const SigSlot::Implementation::Address &SIGSLOT_address) const\
        {\
            return SigSlot::Implementation::ContainSender(SIGSLOT__Object,SIGSLOT_address);\
        }\
\
        bool ContainReceiver(const SigSlot::Implementation::Address &SIGSLOT_address) const\
        {\
            return SigSlot::Implementation::ContainReceiver(SIGSLOT__Object,SIGSLOT_address);\
        }\
    public:\
        void MoveToThread(const std::thread &SIGSLOT_target) noexcept\
        {\
            SIGSLOT__Object.MoveToThread(SIGSLOT_target.get_id());\
        }\
\
        void MoveToThread(const std::thread::id &SIGSLOT_id) noexcept\
        {\
            SIGSLOT__Object.MoveToThread(SIGSLOT_id);\
        }\
\
        void MoveToThread(const SigSlot::Thread &SIGSLOT_other) noexcept\
        {\
            SIGSLOT__Object.MoveToThread(SIGSLOT_other.GetId());\
        }\
\
        std::thread::id ThreadId() noexcept\
        {\
            return SIGSLOT__Object.ThreadId();\
        }\
    private:
#endif //SIGSLOT_SIGSLOT_HPP

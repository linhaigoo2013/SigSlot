#include <iostream>
#include "ParameterCrop.hpp"

using namespace SigSlot::Implementation;

struct Key  
{  
    int value = 10;  
};  

int main()  
{  
    std::function<void(int,char,Key)> func = [](int a, char b, Key key)  
    {  
        std::cout << "参数是" << a << " " << b << " " << key.value << std::endl;  
    };  
    std::tuple<int,int,char,int,float,Key> args = {1, 2, 'a', 4, 3.0f, Key()};  
    TupleTake(func,args);
    //正确打印结果 1,a,10
    return 0;
}



int main()  
{  
    std::function<void(char,int)> func = [](char a,int b)  
    {  
        std::cout << "参数是" << a << " " << b << " " << std::endl;  
    };  
    std::tuple<int,int,char> args ={1, 2, 'a'};  
    TupleTake(func,args);
    //error: static assertion failed: slot function parameters and signal parameters do not match
    return 0;
}

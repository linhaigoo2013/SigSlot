#include <iostream>
#include "EventHandler.hpp"

using namespace SigSlot::Implementation;

void print()  
{  
    std::cout << "我是普通函数没有参数" << std::endl;  
}  

class Button  
{  
public:  
    void print(char a)  
    {  
        std::cout << "类成员函数，参数是：" << a << std::endl;  
    }  
};  

int main()  
{  
    EventHandler<void, std::tuple<int>, int, char> func  
    ([](int a)  
    {  
          std::cout << "我是lambda表达式参数是" << a << std::endl;  
    });  
    func(1, 'a');  
    EventHandler<void, std::tuple<>, int, char> func1(print);  
    func1(1, 'a');  
    EventHandler<Button, std::tuple<char>, int, char> func2(new Button(), &Button::print);  
    func2(1, 'a');  

    EventHandlerInterface<int, char> *handler = &func;  
    (*handler)(1, 'a');  
    handler = &func1;  
    (*handler)(1, 'a');  
    handler = &func2;  
    (*handler)(1, 'a');  
    return 0;  
}

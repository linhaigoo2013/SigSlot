#include <iostream>
#include "Event2.hpp"
#include <string>

using namespace SigSlot;
void print(int x, int y)
{
    std::cout << "鼠标的坐标是x：" << x << "y是：" << y << std::endl;
}

class Button :public Object
{
    int id;
public:
    Event<int, const char*,std::string> Clicked;
    Button(int i) :id(i){}
    void print(int a)
    {
        std::cout << "对象ID是：" << id << std::endl;
    }
    void SetId(int num)
    {
        id = num;
    }
};

class Label :public Object
{
public:
    void print(int a)
    {
        std::cout << "我是label的打印函数，参数是：" << a << std::endl;
    }
};
int main()
{
    Button* button1 = new Button(1);
    Label* label = new Label();
    connect(button1, &Button::Clicked, label, &Label::print);
    connect(button1, &Button::Clicked, label, &Label::print);
    connect(button1, &Button::Clicked, label, [=](const char* text)
    {
        std::cout << "成功绑定lambda函数啦,这里是1号,打印的信息是：" << text << std::endl;
    });
    connect(button1, &Button::Clicked, label, [&](std::string text)
    {
        std::cout << "成功绑定lambda函数啦,这里是2号，打印的信息是：" << text << std::endl;
    });
    button1->Clicked.emit(1, "按钮被点击啦","这个是std::string哦");
    std::cout << "----------------" << std::endl;
    disconnect(button1, &Button::Clicked, label, &Label::print);
    button1->Clicked.emit(1, "按钮被点击啦", "这个是std::string哦");
    delete button1;
    delete label;
    std::cout << "----------------" << std::endl;
    return 0;
}

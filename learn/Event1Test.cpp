#include <iostream>
#include "Event1.hpp"

using namespace SigSlot;

void print(int x, int y)
{
    std::cout << "鼠标的坐标是x：" << x << "y是：" << y << std::endl;
}

class Button
{
public:
    void print(int a)
    {
        std::cout << "类成员函数，参数是：" << a << std::endl;
    }
    Event<int, int> Clicked;
};

class Label
{
public:
    void print(int a)
    {
        std::cout << "类成员函数，参数是：" << a << std::endl;
    }
};

int main()
{
    Button *button = new Button();
    Label *label = new Label();
    button->Clicked.Attach(print);
    button->Clicked.Attach(button, &Button::print);
    button->Clicked.Attach(label, [](int x, int y)
    {
        std::cout << "lambda表达式，x：" << x << "y是：" << y << std::endl;
    });
    button->Clicked(1, 2);
    std::cout << "------------------------" << std::endl;
    button->Clicked.Detach(print);
    button->Clicked(1, 2);
    std::cout << "------------------------" << std::endl;
    button->Clicked.Detach(button, &Button::print);
    button->Clicked(1, 2);
    std::cout << "------------------------" << std::endl;
    button->Clicked.Clear();
    button->Clicked(1, 2);
    delete button;
    delete label;
    return 0;
}

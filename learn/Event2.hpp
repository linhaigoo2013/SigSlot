#ifndef SIGSLOT_EVENT2_HPP
#define SIGSLOT_EVENT2_HPP

#include "EventHandler.hpp"

#include <map>
#include <functional>
#include <tuple>
#include <shared_mutex>
#include <condition_variable>

namespace SigSlot
{
    class Object;

    struct Address
    {
        void *object;
        void *function;

        Address(void *object, void *function) : object(object), function(function)
        {

        }

        template<typename T, typename ...Args>
        Address(Object *receiver, void(T::* handler)(Args...))
        {
            object = receiver;
            function = nullptr;
            memcpy(&function, &handler, sizeof(void *));
        }

        template<typename T, typename ...Args>
        Address(Object *receiver, void(T::* handler)(Args...) const)
        {
            object = receiver;
            function = nullptr;
            memcpy(&function, &handler, sizeof(void *));
        }

        template<typename T, typename U>
        Address(Object *receiver, U T::* handler)
        {
            object = receiver;
            function = nullptr;
            memcpy(&function, &handler, sizeof(void *));
        }

        template<typename ...Args>
        explicit Address(void(*handler)(Args...))
        {
            object = nullptr;
            function = nullptr;
            memcpy(&function, &handler, sizeof(void *));
        }

        bool operator==(const Address &other) const
        {
            return other.object == object && other.function == function;
        }

    };

    //hash 函数计算hash值
    struct AddressHash
    {
        std::size_t operator()(const Address &addr) const
        {
            std::size_t h1 = std::hash<void *>{}(addr.object);
            std::size_t h2 = std::hash<void *>{}(addr.function);
            return h1 ^ (h2 << 1);
        }
    };

    template<typename ...Args>
    class Event
    {
        using Handler = Implementation::EventHandlerInterface<Args...> *;
    private:
        std::unordered_map<Address, Handler, AddressHash> m_Handlers;
        mutable std::shared_mutex m_Mutex;
    private:
        void AddHandler(const Address &address, Handler handler)
        {
            std::unique_lock<std::shared_mutex> lock(m_Mutex);
            if (!m_Handlers.count(address))
            {
                m_Handlers.insert(std::make_pair(address, handler));
            }
        }

        void RemoveHandler(const Address &address)
        {
            std::unique_lock<std::shared_mutex> lock(m_Mutex);
            if (m_Handlers.count(address))
            {
                delete m_Handlers[address];
                m_Handlers.erase(address);
            }
        }

        //处理仿函数的接口
        template<typename T, typename U, typename ...TrueArgs>
        Address ImitationFunctionHelper(T *object, U &&t, void(std::decay_t<U>::* func)(TrueArgs...))
        {
            Address address = Address(object, func);
            Handler handler = new Implementation::EventHandler<void, std::tuple<TrueArgs...>, Args...>(std::forward<U>(t));
            AddHandler(address, handler);
            return address;
        }

        template<typename T, typename U, typename ...TrueArgs>
        Address ImitationFunctionHelper(T *object, U &&t, void(std::decay_t<U>::* func)(TrueArgs...) const)
        {
            Address address = Address(object, func);
            Handler handler = new Implementation::EventHandler<void, std::tuple<TrueArgs...>, Args...>(std::forward<U>(t));
            AddHandler(address, handler);
            return address;
        }

    public:
        void emit(const Args &... args)
        {
            std::shared_lock<std::shared_mutex> lock(m_Mutex);
            for (auto &&handler: m_Handlers)
            {
                (*handler.second)(args...);
            }
        }

        //析构函数记得清除资源
        ~Event()
        {
            for (auto &&handler: m_Handlers)
            {
                delete handler.second;
            }
        }

    public:
        //连接普通成员函数
        template<typename T, typename U, typename ...EmitArgs, typename ...TrueArgs>
        friend void connect(T *sender, Event<EmitArgs...> T::* event, U *receiver, void(U::* func)(TrueArgs...));

        template<typename T, typename U, typename ...EmitArgs, typename ...TrueArgs>
        friend void disconnect(T *sender, Event<EmitArgs...> T::* event, U *receiver, void(U::* func)(TrueArgs...));

        //连接全局函数，静态函数
        template<typename T, typename ...EmitArgs, typename ...TrueArgs>
        friend void connect(T *sender, Event<EmitArgs...> T::* event, void(*func)(TrueArgs...));

        template<typename T, typename ...EmitArgs, typename ...TrueArgs>
        friend void disconnect(T *sender, Event<EmitArgs...> T::* event, void(*func)(TrueArgs...));

        //连接仿函数，lambda
        template<typename T, typename U, typename Lambda, typename ...EmitArgs>
        friend void connect(T *sender, Event<EmitArgs...> T::* event, U *receiver, Lambda &&lambda);

    };


    class Object
    {
        using Connection = std::map<void *, std::function<void()>>;
    private:
        //注意一个订阅者可以使用不同的函数取订阅这个对象，但是存的都是同一个事件，所以用vector存起来
        std::unordered_map<Address, Connection, AddressHash> m_SendersList;
        std::unordered_map<Address, std::function<void()>, AddressHash> m_ReceiversList;
        mutable std::shared_mutex m_Mutex;
    private:
        void AddSender(const Address &senderaddress, void *functionAddress, const std::function<void()> &func)
        {
            std::unique_lock<std::shared_mutex> lock(m_Mutex);
            m_SendersList[senderaddress].insert(std::make_pair(functionAddress, func));
        }

        void AddReceiver(const Address &receiverAddress, const std::function<void()> &func)
        {
            std::unique_lock<std::shared_mutex> lock(m_Mutex);
            m_ReceiversList[receiverAddress] = func;

        }

        void RemoveSender(const Address &senderAddress, void *functionAddress)
        {
            std::unique_lock<std::shared_mutex> lock(m_Mutex);
            if (m_SendersList.count(senderAddress))
            {
                m_SendersList.at(senderAddress).erase(functionAddress);
            }
        }

        void RemoveReceiver(const Address &address)
        {
            std::unique_lock<std::shared_mutex> lock(m_Mutex);
            m_ReceiversList.erase(address);

        }

        bool ContainSender(const Address &address)
        {
            std::shared_lock<std::shared_mutex> lock(m_Mutex);
            return m_SendersList.count(address);
        }

        bool ContainReceiver(const Address &address) const
        {
            std::shared_lock<std::shared_mutex> lock(m_Mutex);
            return m_ReceiversList.count(address);
        }

    public:
        virtual ~Object()
        {
            std::shared_lock<std::shared_mutex> lock(m_Mutex);
            for (auto &pair: m_SendersList)
            {
                for (auto &func: pair.second)
                {
                    func.second();
                }
            }
            for (auto &receiver: m_ReceiversList)
            {
                (receiver.second)();
            }
        }

        template<typename T, typename U, typename ...EmitArgs, typename ...TrueArgs>
        friend void connect(T *sender, Event<EmitArgs...> T::* event, U *receiver, void(U::* func)(TrueArgs...));

        template<typename T, typename U, typename ...EmitArgs, typename ...TrueArgs>
        friend void disconnect(T *sender, Event<EmitArgs...> T::* event, U *receiver, void(U::* func)(TrueArgs...));

        template<typename T, typename U, typename Lambda, typename ...EmitArgs>
        friend void connect(T *sender, Event<EmitArgs...> T::* event, U *receiver, Lambda &&lambda);
    };


    template<typename T, typename U, typename... EmitArgs, typename... TrueArgs>
    inline void connect(T *sender, Event<EmitArgs...> T::* event, U *receiver, void (U::* func)(TrueArgs...))
    {
        Address ReceiverAddress(receiver, func);
        Address SenderAddress(sender, event);
        if (!(sender->ContainReceiver(ReceiverAddress) && receiver->ContainSender(SenderAddress)))
        {
            //保证连接的可靠性，存入事件
            auto handler = new Implementation::EventHandler<U, std::tuple<TrueArgs...>, EmitArgs...>(receiver, func);
            (sender->*event).AddHandler(ReceiverAddress, handler);
            //订阅者和发布者同时保存对方的信息，在销毁的时候，通知对方把自己删除，后面都是类似的处理
            sender->AddReceiver(ReceiverAddress, [=]()
            {
                receiver->RemoveSender(SenderAddress, ReceiverAddress.function);
            });
            receiver->AddSender(SenderAddress, ReceiverAddress.function, [=]()
            {
                (sender->*event).RemoveHandler(ReceiverAddress);
                sender->RemoveReceiver(ReceiverAddress);
            });
        }

    }

    template<typename T, typename U, typename ...EmitArgs, typename ...TrueArgs>
    inline void disconnect(T *sender, Event<EmitArgs...> T::* event, U *receiver, void(U::* func)(TrueArgs...))
    {
        Address ReceiverAddress(receiver, func);
        Address SenderAddress(sender, event);
        if (sender->ContainReceiver(ReceiverAddress) && receiver->ContainSender(SenderAddress))
        {
            (sender->*event).RemoveHandler(ReceiverAddress);
            sender->RemoveReceiver(ReceiverAddress);
            receiver->RemoveSender(SenderAddress, ReceiverAddress.function);
        }
    }

    template<typename T, typename ...EmitArgs, typename ...TrueArgs>
    inline void connect(T *sender, Event<EmitArgs...> T::* event, void(*func)(TrueArgs...))
    {
        auto handler = new Implementation::EventHandler<void, std::tuple<TrueArgs...>, EmitArgs...>(func);
        (sender->*event).AddHandler(Address(nullptr, func), handler);
    }

    template<typename T, typename ...EmitArgs, typename ...TrueArgs>
    inline void disconnect(T *sender, Event<EmitArgs...> T::* event, void(*func)(TrueArgs...))
    {
        (sender->*event).RemoveHandler(Address(nullptr, func));
    }

    template<typename T, typename U, typename Lambda, typename ...EmitArgs>
    inline void connect(T *sender, Event<EmitArgs...> T::* event, U *receiver, Lambda &&lambda)
    {
        Address SenderAddress(sender, event);
        //借用之前的lambda辅助函数来进行lambda的提取
        Address ReceiverAddress = (sender->*event).ImitationFunctionHelper(receiver, lambda, &Lambda::operator());
        sender->AddReceiver(ReceiverAddress, [=]()
        {
            receiver->RemoveSender(SenderAddress, ReceiverAddress.function);
        });
        receiver->AddSender(SenderAddress, ReceiverAddress.function, [=]()
        {
            (sender->*event).RemoveHandler(ReceiverAddress);
            sender->RemoveReceiver(ReceiverAddress);
        });
    }
}
#endif //SIGSLOT_EVENT2_HPP

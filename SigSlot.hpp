#ifndef EVENT_SIGSLOT_HPP
#define EVENT_SIGSLOT_HPP

#include <tuple>
#include <functional>
#include <type_traits>
#include <map>
#include <mutex>
#include <atomic>
#include <unordered_map>
#include <thread>
#include <mutex>
#include <atomic>
#include <functional>
#include <queue>
#include <optional>
#include <thread>
#include <condition_variable>
#include <shared_mutex>
#include <memory>

namespace SigSlot
{
    //声明
    enum SIGSLOT
    {
        AutoConnection,
        DirectConnection,
        QueuedConnection,
        BlockingQueuedConnection,
    };

    class Object;

    template<typename ...Args>
    class Event;

    class EventLoop;

    static EventLoop *GetEventLoop(std::thread::id id = std::this_thread::get_id());

    static EventLoop *GetEventLoop(const std::thread &thread);

    //普通成员函数
    template<typename Sender, typename Receiver, typename T, typename U, typename ...SignalArgs, typename ...SlotArgs>
    static constexpr void connect(Sender *sender, Event<SignalArgs...> T::* event, Receiver *receiver, void(U::* handler)(SlotArgs...), SIGSLOT type = SIGSLOT::AutoConnection);

    template<typename Sender, typename Receiver, typename T, typename U, typename ...SignalArgs, typename ...SlotArgs>
    static constexpr void disconnect(Sender *sender, Event<SignalArgs...> T::* event, Receiver *receiver, void(U::* handler)(SlotArgs...));

    template<typename Sender, typename Receiver, typename T, typename U, typename ...SignalArgs, typename ...SlotArgs>
    static constexpr void connect(Sender *sender, Event<SignalArgs...> T::* event, Receiver *receiver, void(U::* handler)(SlotArgs...) const, SIGSLOT type = SIGSLOT::AutoConnection);

    template<typename Sender, typename Receiver, typename T, typename U, typename ...SignalArgs, typename ...SlotArgs>
    static constexpr void disconnect(Sender *sender, Event<SignalArgs...> T::* event, Receiver *receiver, void(U::* handler)(SlotArgs...) const);

    //类成员函数
    template<typename Sender, typename Receiver, typename T, typename Lambda, typename ...SignalArgs>
    static constexpr void connect(Sender *sender, Event<SignalArgs...> T::* event, Receiver *receiver, Lambda &&lambda, SIGSLOT type = SIGSLOT::AutoConnection);

    //全局函数，静态函数
    template<typename Sender, typename T, typename ...SignalArgs, typename ...SlotArgs>
    static constexpr void connect(Sender *sender, Event<SignalArgs...> T::* event, void(*handler)(SlotArgs...));

    template<typename Sender, typename T, typename ...SignalArgs, typename ...SlotArgs>
    static constexpr void disconnect(Sender *sender, Event<SignalArgs...> T::* event, void(*handler)(SlotArgs...));

}
namespace SigSlot::Implementation
{
    //参数裁剪相关
    template<typename Subset, typename Superset>
    struct is_subset_of;

    template<typename T, typename ...Subset, typename U, typename ...Superset>
    struct is_subset_of<std::tuple<T, Subset...>, std::tuple<U, Superset...>>
    {
        constexpr static bool value = is_subset_of<std::tuple<T, Subset...>, std::tuple<Superset...>>::value;
    };

    template<typename T, typename ...Subset, typename ...Superset>
    struct is_subset_of<std::tuple<T, Subset...>, std::tuple<T, Superset...>>
    {
        constexpr static bool value = is_subset_of<std::tuple<Subset...>, std::tuple<Superset...>>::value;
    };

    template<typename ...Superset>
    struct is_subset_of<std::tuple<>, std::tuple<Superset...>>
    {
        constexpr static bool value = true;
    };

    template<typename ...Subset>
    struct is_subset_of<std::tuple<Subset...>, std::tuple<>>
    {
        constexpr static bool value = false;
    };

    template<>
    struct is_subset_of<std::tuple<>, std::tuple<>>
    {
        constexpr static bool value = true;
    };

    template<int N, typename T, typename Tuple>
    struct find_next_index;

    template<int N, typename T, typename U, typename ...Args>
    struct find_next_index<N, T, std::tuple<U, Args...>>
    {
        constexpr static int value = find_next_index<N - 1, T, std::tuple<Args...>>::value + 1;
    };
    template<typename T, typename ...Args>
    struct find_next_index<-1, T, std::tuple<T, Args...>>
    {
        constexpr static int value = 0;
    };
    template<typename T, typename U, typename ...Args>
    struct find_next_index<-1, T, std::tuple<U, Args...>>
    {
        constexpr static int value = find_next_index<-1, T, std::tuple<Args...>>::value + 1;
    };

    template<int ...Args>
    struct int_list
    {
        using type = int_list<Args...>;

        template<typename Tuple>
        constexpr static auto MakeTupleByList(const Tuple &target) noexcept
        {
            return std::make_tuple(std::get<Args>(target)...);
        }
    };

    template<int N, typename list>
    struct list_prepend;

    template<int N, int ...Args>
    struct list_prepend<N, int_list<Args...>>
    {
        using result = int_list<N, Args...>;
    };

    template<int N, typename Subset, typename Superset>
    struct find_all_index;

    template<int N, typename T, typename ...Subset, typename ...Superset>
    struct find_all_index<N, std::tuple<T, Subset...>, std::tuple<Superset...>>
    {
        using value = typename list_prepend<find_next_index<N, T, std::tuple<Superset...>>::value,
                typename find_all_index<find_next_index<N, T, std::tuple<Superset...>>::value, std::tuple<Subset...>, std::tuple<Superset...>>::value>::result;
    };

    template<int N, typename ...Superset>
    struct find_all_index<N, std::tuple<>, std::tuple<Superset...>>
    {
        using value = int_list<>;
    };


    template<typename ...Subset, typename ...Superset>
    constexpr auto TupleTake(const std::function<void(Subset...)> &func, const std::tuple<Superset...> &target) noexcept
    {
        using SubsetTuple = std::tuple<std::decay_t<Subset>...>;
        using SupersetTuple = std::tuple<std::decay_t<Superset>...>;

        if constexpr (is_subset_of<SubsetTuple, SupersetTuple>::value)
        {
            using IndexList = typename find_all_index<-1, SubsetTuple, SupersetTuple>::value;
            std::apply(func, IndexList::MakeTupleByList(target));
        }
    }

    template<typename T>
    struct lambda_traits;

    template<typename T, typename U,typename ...Args>
    struct lambda_traits<T(U::*)(Args...)>
    {
        using return_type = T;
        using parameter_type = std::tuple<std::decay_t<Args>...>;
    };

    template<typename T, typename U,typename ...Args>
    struct lambda_traits<T(U::*)(Args...) const>
    {
        using return_type = T;
        using parameter_type = std::tuple<std::decay_t<Args>...>;
    };

    template<typename T, typename U>
    struct is_object
    {
        constexpr static bool value = false;
    };

    template<typename T>
    struct is_object<T, decltype(std::declval<T>().ThreadId())>
    {
        constexpr static bool value = true;
    };

    //函数类型擦除相关
    template<typename ...Args>
    class EventHandlerInterface
    {
    public:
        EventHandlerInterface(const EventHandlerInterface &) = delete;
        EventHandlerInterface &operator=(const EventHandlerInterface &event) = delete;
        EventHandlerInterface() = default;
        virtual ~EventHandlerInterface() = default;

        //重载()是为了方便之后调用
        virtual void operator()(const Args &...args) = 0;
    };

    //提供两种特化,不用存储this指针的一类
    //另一类如类成员函数是需要对象指针的,所以把类对象指针也存起来
    template<typename T, typename Tuple, typename ...Args>
    class EventHandler final : public EventHandlerInterface<Args...> {};


    template<typename ...SlotArgs, typename ...Args>
    class EventHandler<void, std::tuple<SlotArgs...>, Args...> final : public EventHandlerInterface<Args...>
    {
    private:
        std::function<void(SlotArgs...)> m_Handler;
    public:
        EventHandler(const EventHandler &eventHandler) = delete;
        EventHandler &operator=(const EventHandler &eventHandler) = delete;
        EventHandler(EventHandler &&eventHandler) = delete;
        EventHandler &operator=(EventHandler &&eventHandler) = delete;

        template<typename Callable>
        explicit EventHandler(Callable &&func) noexcept
        {
            m_Handler = std::forward<Callable>(func);
        }

        void operator()(const Args &... args) final
        {
            TupleTake(m_Handler, std::make_tuple(args...));
        }
    };


    template<typename T, typename ...SlotArgs, typename ...Args>
    class EventHandler<T, std::tuple<SlotArgs...>, Args...> final : public EventHandlerInterface<Args...>
    {
        using FunctionPointer = void (T::*)(SlotArgs...);
        using ConstFunctionPointer = void (T::*)(SlotArgs...) const;
    private:
        T *m_Receiver;
        std::function<void(T *, SlotArgs...)> m_Handler;
    public:
        EventHandler(const EventHandler &eventHandler) = delete;
        EventHandler &operator=(const EventHandler &eventHandler) = delete;
        EventHandler(EventHandler &&eventHandler) = delete;
        EventHandler &operator=(EventHandler &&eventHandler) = delete;

        EventHandler(T *receiver, FunctionPointer handler) noexcept
        {
            m_Receiver = receiver;
            m_Handler = handler;
        }

        EventHandler(T *receiver, ConstFunctionPointer handler) noexcept
        {
            m_Receiver = receiver;
            m_Handler = handler;
        }

        void operator()(const Args &...args) final
        {
            TupleTake(m_Handler, std::make_tuple(m_Receiver, args...));
        }
    };

    //处理不同编译器指针大小差异,不同编译器类成员函数指针大小不一样
    //msvc是8字节，mingw是16字节
    template<bool Is64, std::size_t size>
    struct ClassFunctionPointer;

    template<>
    struct ClassFunctionPointer<true, 8>
    {
        void *address = nullptr;

        bool operator==(const ClassFunctionPointer &other) const
        {
            return address == other.address;
        }

        bool operator<(const ClassFunctionPointer &other) const
        {
            return address < other.address;
        }
    };

    template<>
    struct ClassFunctionPointer<true, 16>
    {
        void *address[2] = {nullptr, nullptr};

        bool operator==(const ClassFunctionPointer &other) const
        {
            return address[0] == other.address[0] && address[1] == other.address[1];
        }

        bool operator<(const ClassFunctionPointer &other) const
        {
            return address[0] < other.address[0] || (address[0] == other.address[0] && address[1] < other.address[1]);
        }
    };

    template<>
    struct ClassFunctionPointer<false, 4>
    {
        void *address = nullptr;

        bool operator==(const ClassFunctionPointer &other) const
        {
            return address == other.address;
        }

        bool operator<(const ClassFunctionPointer &other) const
        {
            return address < other.address;
        }
    };

    template<>
    struct ClassFunctionPointer<false, 8>
    {
        void *address[2] = {nullptr, nullptr};

        bool operator==(const ClassFunctionPointer &other) const
        {
            return address[0] == other.address[0] && address[1] == other.address[1];
        }

        bool operator<(const ClassFunctionPointer &other) const
        {
            return address[1] < other.address[1] || (address[1] == other.address[1] && address[0] < other.address[0]);
        }
    };

    using SIGSLOT_ClassFunctionPtr = ClassFunctionPointer<sizeof(void *) == 8, sizeof(decltype(&EventHandlerInterface<>::operator()))>;

    //存储函数地址相关,object存放对象指针，function存放函数指针
    struct Address
    {
        void *object;
        SIGSLOT_ClassFunctionPtr function;

        template<typename T, typename U, typename ...Args>
        constexpr Address(T *receiver, void(U::* handler)(Args...)) noexcept
        {
            object = receiver;
            memcpy(&function, &handler, sizeof(SIGSLOT_ClassFunctionPtr));
        }

        template<typename T, typename U, typename ...Args>
        constexpr Address(T *receiver, void(U::* handler)(Args...) const) noexcept
        {
            object = receiver;
            memcpy(&function, &handler, sizeof(SIGSLOT_ClassFunctionPtr));
        }

        template<typename T, typename U, typename ...Args>
        constexpr Address(T *sender, Event<Args...> U::* event) noexcept
        {
            object = sender;
            memcpy(&function, &event, sizeof(event));
        }

        template<typename ...Args>
        constexpr explicit Address(void(*handler)(Args...)) noexcept
        {
            object = nullptr;
            memcpy(&function, &handler, sizeof(void (*)()));
        }

        bool operator==(const Address &other) const noexcept
        {
            return other.object == object && other.function == function;
        }

    };

    //hash 函数计算hash值
    struct AddressHash
    {
        std::size_t operator()(const Address &address) const
        {
            std::size_t h1 = std::hash<void *>{}(address.object);
            constexpr static bool condition = (sizeof(void (Address::*)()) == sizeof(void *) ||sizeof(void (Address::*)()) == sizeof(void *) * 2);
            static_assert(condition, "SigSlot can't work");
            //根据不同编译器指针大小计算hash值
            if constexpr (sizeof(address.function) == sizeof(void *))
            {
                std::size_t h2 = std::hash<void *>{}((void *) address.function.address);
                return h1 ^ (h2 << 1);
            }
            else if constexpr (sizeof(address.function) == sizeof(void *) * 2)
            {
                void *functionAddress[2] = {nullptr, nullptr};
                memcpy(functionAddress, &address.function, sizeof(void *) * 2);
                std::size_t h2 = std::hash<void *>{}(functionAddress[0]);
                std::size_t h3 = std::hash<void *>{}(functionAddress[1]);
                return h1 ^ (h2 << 1) ^ (h3 << 2);
            }
        }
    };

    template<typename Callable>
    static void AddSender(Object &object, const Address &senderAddress, const SIGSLOT_ClassFunctionPtr &functionAddress, Callable &&func);

    template<typename Callable>
    static void AddReceiver(Object &object, const Address &receiverAddress, Callable &&func);

    static void RemoveSender(Object &object, const Address &senderAddress, const SIGSLOT_ClassFunctionPtr &functionAddress);

    static void RemoveReceiver(Object &object, const Address &address);

    static bool ContainSender(const Object &object, const Address &address);

    static bool ContainReceiver(const Object &object, const Address &address);

    // 事件循环的管理相关

    class EventLoopManager
    {
    private:
        std::unordered_map<std::thread::id, EventLoop *> m_EventLoops;
        std::mutex m_Mutex;

        EventLoopManager() = default;
        ~EventLoopManager() = default;
    public:
        EventLoopManager(const EventLoopManager &) = delete;
        EventLoopManager &operator=(const EventLoopManager &) = delete;

        static EventLoopManager *GetInstance() noexcept
        {
            static EventLoopManager instance;
            return &instance;
        }

        void AddEventLoop(EventLoop *loop)
        {
            std::unique_lock<std::mutex> lock(m_Mutex);
            m_EventLoops.insert(std::make_pair(std::this_thread::get_id(), loop));
        }

        void RemoveEventLoop()
        {
            std::unique_lock<std::mutex> lock(m_Mutex);
            if (m_EventLoops.count(std::this_thread::get_id()))
            {
                m_EventLoops.erase(std::this_thread::get_id());
            }
        }

        EventLoop *GetEventLoop(std::thread::id id)
        {
            std::unique_lock<std::mutex> lock(m_Mutex);
            if (m_EventLoops.count(id))
            {
                return m_EventLoops[id];
            }
            return nullptr;
        }
    };

}
namespace SigSlot
{
    class EventLoop
    {
    private:
        std::mutex m_Mutex;
        std::condition_variable m_Condition;
        std::deque<std::function<void()>> m_Messages;
        std::atomic<bool> m_Quit{};
    public:
        EventLoop()
        {
            Implementation::EventLoopManager::GetInstance()->AddEventLoop(this);
            m_Quit = false;
        }

        ~EventLoop()
        {
            Implementation::EventLoopManager::GetInstance()->RemoveEventLoop();
            Quit();
        }

        template<typename Callable>
        void PostEvent(Callable &&func)
        {
            std::unique_lock<std::mutex> lock(m_Mutex);
            m_Messages.push_back(std::forward<Callable>(func));
            m_Condition.notify_all();
        }

        template<typename Callable>
        void SendEvent(Callable &&func)
        {
            std::unique_lock<std::mutex> lock(m_Mutex);
            std::atomic<bool> Finished = false;
            m_Messages.push_back([func = std::forward<Callable>(func), &Finished]()
            {
                func();
                Finished = true;
            });
            m_Condition.notify_all();
            m_Condition.wait(lock, [&Finished]
            {
                return Finished.load();
            });
        }

        void Update()
        {
            std::unique_lock<std::mutex> lock(m_Mutex);
            m_Condition.wait(lock, [this]
            {
                    return !m_Messages.empty() || m_Quit;
            });
            std::optional<std::function<void()>> func;
            if (!m_Messages.empty())
            {
                func = std::move(m_Messages.front());
		m_Messages.pop_front(); 
            }
            lock.unlock();
            if (func.has_value())
            {
                func.value()();
            }
            m_Condition.notify_all();
        }

        void Run()
        {
            m_Quit = false;
            while (!m_Quit)
            {
                Update();
            }
        }

        void Quit()
        {
            m_Quit = true;
            m_Condition.notify_all();
        }
	    
    };

    class Thread
    {
    private:
        std::atomic<std::thread::id> id;
        std::thread thread;
    public:
        Thread(const Thread &other) = delete;
        Thread &operator=(const Thread &other) = delete;

        Thread(Thread &&other) noexcept
        {
            id = other.id.load();
            thread = std::move(other.thread);
        }

        Thread &operator=(Thread &&other) noexcept
        {
            id = other.id.load();
            thread = std::move(other.thread);
            return *this;
        }

        template<typename Callable, typename ...Args>
        explicit Thread(Callable &&func, Args &&... args) :thread([&]()
        {
            id = std::this_thread::get_id();
            std::forward<Callable>(func)(std::forward<Args>(args)...);
        })
        {
            while (GetEventLoop(id) == nullptr)
            {
                std::this_thread::yield();
            }
        }
        
        ~Thread()
        {
            thread.detach();
        }
        
        std::thread::id GetID() const
        {
            return id.load();
        }
    };

    template<typename ...Args>
    class Event
    {
    private:
        using Address = Implementation::Address;
        using AddressHash = Implementation::AddressHash;
        struct Handler
        {
            std::thread::id id;
            std::shared_ptr<Implementation::EventHandlerInterface<Args...>> handler;
            SIGSLOT type;
        };
    private:
        std::unordered_map<Address, Handler, AddressHash> m_Handlers;
        mutable std::shared_mutex m_Mutex;
    private:
        void AddHandler(const Address &address, const Handler &handler)
        {
            std::unique_lock<std::shared_mutex> lock(m_Mutex);
            if (!m_Handlers.count(address))
            {
                m_Handlers.insert(std::make_pair(address, handler));
            }
        }

        void RemoveHandler(const Address &address)
        {
            std::unique_lock<std::shared_mutex> lock(m_Mutex);
            if (m_Handlers.count(address))
            {
                m_Handlers.erase(address);
            }
        }

        //处理仿函数的接口
        template<typename T, typename U, typename ...SlotArgs>
        Address ImitationFunctionHelper(T *object, U &&t, void(std::decay_t<U>::* func)(SlotArgs...), SIGSLOT type)
        {
            constexpr bool is_object_v = Implementation::is_object<T, std::thread::id>::value;
            Address address = Address(object, func);
            Handler v_handler;
            v_handler.id = std::this_thread::get_id();
            v_handler.handler = std::make_shared<Implementation::EventHandler<void, std::tuple<SlotArgs...>, Args...>>(std::forward<U>(t));
            v_handler.type = type;
            if constexpr (is_object_v)
            {
                v_handler.id = object->ThreadId();
            }
            AddHandler(address, v_handler);
            return address;
        }

        template<typename T, typename U, typename ...SlotArgs>
        Address ImitationFunctionHelper(T *object, U &&t, void(std::decay_t<U>::* func)(SlotArgs...) const, SIGSLOT type)
        {
            constexpr bool is_object_v = Implementation::is_object<T, std::thread::id>::value;
            Address address = Address(object, func);
            Handler v_handler;
            v_handler.id = std::this_thread::get_id();
            v_handler.handler = std::make_shared<Implementation::EventHandler<void, std::tuple<SlotArgs...>, Args...>>(std::forward<U>(t));
            v_handler.type = type;
            if constexpr (is_object_v)
            {
                v_handler.id = object->ThreadId();
            }
            AddHandler(address, v_handler);
            return address;
        }

    public:
        ~Event()
        {
            m_Handlers.clear();
        }

        void emit(const Args &... args)
        {
            std::shared_lock<std::shared_mutex> lock(m_Mutex);
            for (auto &&element: m_Handlers)
            {
                switch (element.second.type)
                {
                    case SIGSLOT::AutoConnection:
                    {
                        if (element.second.id == std::this_thread::get_id())
                        {
                            (*element.second.handler)(args...);
                        }
                        else
                        {
                            EventLoop *loop = Implementation::EventLoopManager::GetInstance()->GetEventLoop(element.second.id);
                            if (loop != nullptr)
                            {
                                loop->PostEvent([handler = element.second.handler, args...]
                                {
                                    (*handler)(args...);
                                });
                            }
                        }
                        break;
                    }
                    case SIGSLOT::DirectConnection:
                    {
                        (*element.second.handler)(args...);
                        break;
                    }
                    case SIGSLOT::QueuedConnection:
                    {
                        EventLoop *loop = Implementation::EventLoopManager::GetInstance()->GetEventLoop(element.second.id);
                        if (loop != nullptr)
                        {
                            loop->PostEvent([handler = element.second.handler, args...]
                            {
                                (*handler)(args...);
                            });
                        }
                        break;
                    }
                    case SIGSLOT::BlockingQueuedConnection:
                    {
                        EventLoop *loop = Implementation::EventLoopManager::GetInstance()->GetEventLoop(element.second.id);
                        if (loop != nullptr)
                        {
                            loop->SendEvent([handler = element.second.handler, args...]
                            {
                                (*handler)(args...);
                            });
                        }
                        break;
                    }
                }
            }
        }

    public:
        //普通成员函数
        template<typename Sender, typename Receiver, typename T, typename U, typename ...SignalArgs, typename ...SlotArgs>
        friend constexpr void connect(Sender *sender, Event<SignalArgs...> T::* event, Receiver *receiver, void(U::* handler)(SlotArgs...), SIGSLOT type);

        template<typename Sender, typename Receiver, typename T, typename U, typename ...SignalArgs, typename ...SlotArgs>
        friend constexpr void disconnect(Sender *sender, Event<SignalArgs...> T::* event, Receiver *receiver, void(U::* handler)(SlotArgs...));

        template<typename Sender, typename Receiver, typename T, typename U, typename ...SignalArgs, typename ...SlotArgs>
        friend constexpr void connect(Sender *sender, Event<SignalArgs...> T::* event, Receiver *receiver, void(U::* handler)(SlotArgs...) const, SIGSLOT type);

        template<typename Sender, typename Receiver, typename T, typename U, typename ...SignalArgs, typename ...SlotArgs>
        friend constexpr void disconnect(Sender *sender, Event<SignalArgs...> T::* event, Receiver *receiver, void(U::* handler)(SlotArgs...) const);

        //lambda表达式
        template<typename Sender, typename Receiver, typename T, typename Lambda, typename ...SignalArgs>
        friend constexpr void connect(Sender *sender, Event<SignalArgs...> T::* event, Receiver *receiver, Lambda &&lambda, SIGSLOT type);

        //全局函数，静态函数
        template<typename Sender, typename T, typename ...SignalArgs, typename ...SlotArgs>
        friend constexpr void connect(Sender *sender, Event<SignalArgs...> T::* event, void(*handler)(SlotArgs...));

        template<typename Sender, typename T, typename ...SignalArgs, typename ...SlotArgs>
        friend constexpr void disconnect(Sender *sender, Event<SignalArgs...> T::* event, void(*handler)(SlotArgs...));

    };


    class Object
    {
    private:
        using Address = Implementation::Address;
        using AddressHash = Implementation::AddressHash;
        using Connection = std::map<Implementation::SIGSLOT_ClassFunctionPtr, std::function<void()>>;
    private:
        //注意一个订阅者可以使用不同的函数取订阅这个对象，但是存的都是同一个事件，所以用vector存起来
        std::atomic<std::thread::id> m_Id;
        std::unordered_map<Address, Connection, AddressHash> m_SendersList;
        std::unordered_map<Address, std::function<void()>, AddressHash> m_ReceiversList;
        mutable std::shared_mutex m_Mutex;
    private:
        template<typename Callable>
        void AddSender(const Address &senderAddress, const Implementation::SIGSLOT_ClassFunctionPtr &functionAddress, Callable &&func)
        {
            std::unique_lock<std::shared_mutex> lock(m_Mutex);
            m_SendersList[senderAddress].insert(std::make_pair(functionAddress, std::forward<Callable>(func)));
        }

        template<typename Callable>
        void AddReceiver(const Address &receiverAddress, Callable &&func)
        {
            std::unique_lock<std::shared_mutex> lock(m_Mutex);
            m_ReceiversList[receiverAddress] = std::forward<Callable>(func);
        }

        void RemoveSender(const Address &senderAddress, const Implementation::SIGSLOT_ClassFunctionPtr &functionAddress)
        {
            std::unique_lock<std::shared_mutex> lock(m_Mutex);
            if (m_SendersList.count(senderAddress))
            {
                if (m_SendersList.at(senderAddress).count(functionAddress))
                {
                    m_SendersList.at(senderAddress).erase(functionAddress);
                }
            }
        }

        void RemoveReceiver(const Address &address)
        {
            std::unique_lock<std::shared_mutex> lock(m_Mutex);
            if (m_ReceiversList.count(address))
            {
                m_ReceiversList.erase(address);
            }
        }

        bool ContainSender(const Address &address) const
        {
            std::shared_lock<std::shared_mutex> lock(m_Mutex);
            return m_SendersList.count(address);
        }

        bool ContainReceiver(const Address &address) const
        {
            std::shared_lock<std::shared_mutex> lock(m_Mutex);
            return m_ReceiversList.count(address);
        }

    private:
        template<typename Callable>
        friend void Implementation::AddSender(Object &object, const Address &senderAddress, const Implementation::SIGSLOT_ClassFunctionPtr &functionAddress, Callable &&func);

        template<typename Callable>
        friend void Implementation::AddReceiver(Object &object, const Address &receiverAddress, Callable &&func);

        friend void Implementation::RemoveSender(Object &object, const Address &senderAddress, const Implementation::SIGSLOT_ClassFunctionPtr &functionAddress);

        friend void Implementation::RemoveReceiver(Object &object, const Address &address);

        friend bool Implementation::ContainSender(const Object &object, const Address &address);

        friend bool Implementation::ContainReceiver(const Object &object, const Address &address);

    public:
        Object()
        {
            m_Id = std::this_thread::get_id();
        }

        virtual ~Object()
        {
            for (auto &pair: m_SendersList)
            {
                for (auto &func: pair.second)
                {
                    func.second();
                }
            }
            for (auto &receiver: m_ReceiversList)
            {
                (receiver.second)();
            }
        }

        void MoveToThread(const std::thread &target) noexcept
        {
            m_Id = target.get_id();
        }

        void MoveToThread(const std::thread::id &id) noexcept
        {
            m_Id = id;
        }

        void MoveToThread(const Thread &other) noexcept
        {
            m_Id = other.GetID();
        }

        std::thread::id ThreadId() const noexcept
        {
            return m_Id;
        }


        //普通成员函数
        template<typename Sender, typename Receiver, typename T, typename U, typename ...SignalArgs, typename ...SlotArgs>
        friend constexpr void connect(Sender *sender, Event<SignalArgs...> T::* event, Receiver *receiver, void(U::* handler)(SlotArgs...), SIGSLOT type);

        template<typename Sender, typename Receiver, typename T, typename U, typename ...SignalArgs, typename ...SlotArgs>
        friend constexpr void disconnect(Sender *sender, Event<SignalArgs...> T::* event, Receiver *receiver, void(U::* handler)(SlotArgs...));

        template<typename Sender, typename Receiver, typename T, typename U, typename ...SignalArgs, typename ...SlotArgs>
        friend constexpr void connect(Sender *sender, Event<SignalArgs...> T::* event, Receiver *receiver, void(U::* handler)(SlotArgs...) const, SIGSLOT type);

        template<typename Sender, typename Receiver, typename T, typename U, typename ...SignalArgs, typename ...SlotArgs>
        friend constexpr void disconnect(Sender *sender, Event<SignalArgs...> T::* event, Receiver *receiver, void(U::* handler)(SlotArgs...) const);

        //lambda
        template<typename Sender, typename Receiver, typename T, typename Lambda, typename ...SignalArgs>
        friend constexpr void connect(Sender *sender, Event<SignalArgs...> T::* event, Receiver *receiver, Lambda &&lambda, SIGSLOT type);

        //全局函数，静态函数
        template<typename Sender, typename T, typename ...SignalArgs, typename ...SlotArgs>
        friend constexpr void connect(Sender *sender, Event<SignalArgs...> T::* event, void(*handler)(SlotArgs...));

        template<typename Sender, typename T, typename ...SignalArgs, typename ...SlotArgs>
        friend constexpr void disconnect(Sender *sender, Event<SignalArgs...> T::* event, void(*handler)(SlotArgs...));

    };


    template<typename Sender, typename Receiver, typename T, typename U, typename ...SignalArgs, typename ...SlotArgs>
    inline constexpr void connect(Sender *sender, Event<SignalArgs...> T::* event, Receiver *receiver, void (U::* handler)(SlotArgs...), SIGSLOT type)
    {
        using SignalArgsTuple = std::tuple<std::decay_t<SignalArgs>...>;
        using SlotArgsTuple = std::tuple<std::decay_t<SlotArgs>...>;
        static_assert(Implementation::is_subset_of<SlotArgsTuple, SignalArgsTuple>::value, "slot function parameters and signal parameters do not match");

        Implementation::Address ReceiverAddress(receiver, handler);
        Implementation::Address SenderAddress(sender, event);

        using Handler = typename Event<SignalArgs...>::Handler;
        Handler v_handler;
        v_handler.handler = std::make_shared<Implementation::EventHandler<U, std::tuple<SlotArgs...>, SignalArgs...>>((U *) receiver, handler);
        v_handler.type = type;
        v_handler.id = std::this_thread::get_id();

        constexpr bool is_object_v = Implementation::is_object<T, std::thread::id>::value && Implementation::is_object<U, std::thread::id>::value;
        constexpr bool is_not_object_v = !Implementation::is_object<T, std::thread::id>::value && !Implementation::is_object<U, std::thread::id>::value;
        static_assert(is_object_v || is_not_object_v, "Sender and Receiver must both be Object or neither be");
        if constexpr (is_object_v)
        {
            v_handler.id = receiver->ThreadId();
            sender->AddReceiver(ReceiverAddress, [=]()
            {
                receiver->RemoveSender(SenderAddress, ReceiverAddress.function);
            });
            receiver->AddSender(SenderAddress, ReceiverAddress.function, [=]()
            {
                (static_cast<T *>(sender)->*event).RemoveHandler(ReceiverAddress);
                sender->RemoveReceiver(ReceiverAddress);
            });
        }
        (static_cast<T *>(sender)->*event).AddHandler(ReceiverAddress, v_handler);
    }

    template<typename Sender, typename Receiver, typename T, typename U, typename ...SignalArgs, typename ...SlotArgs>
    inline constexpr void disconnect(Sender *sender, Event<SignalArgs...> T::* event, Receiver *receiver, void(U::* handler)(SlotArgs...))
    {
        using SignalArgsTuple = std::tuple<std::decay_t<SignalArgs>...>;
        using SlotArgsTuple = std::tuple<std::decay_t<SlotArgs>...>;
        static_assert(Implementation::is_subset_of<SlotArgsTuple, SignalArgsTuple>::value, "slot function parameters and signal parameters do not match");

        Implementation::Address ReceiverAddress(receiver, handler);
        Implementation::Address SenderAddress(sender, event);
        (static_cast<T *>(sender)->*event).RemoveHandler(ReceiverAddress);

        constexpr bool is_object_v = Implementation::is_object<T, std::thread::id>::value && Implementation::is_object<U, std::thread::id>::value;
        constexpr bool is_not_object_v = !Implementation::is_object<T, std::thread::id>::value && !Implementation::is_object<U, std::thread::id>::value;
        static_assert(is_object_v || is_not_object_v, "Sender and Receiver must both be Object or neither be");
        if constexpr (is_object_v)
        {
            if (sender->ContainReceiver(ReceiverAddress) && receiver->ContainSender(SenderAddress))
            {
                sender->RemoveReceiver(ReceiverAddress);
                receiver->RemoveSender(SenderAddress, ReceiverAddress.function);
            }
        }
    }

    template<typename Sender, typename Receiver, typename T, typename U, typename ...SignalArgs, typename ...SlotArgs>
    inline constexpr void connect(Sender *sender, Event<SignalArgs...> T::* event, Receiver *receiver, void (U::* handler)(SlotArgs...) const, SIGSLOT type)
    {
        using SignalArgsTuple = std::tuple<std::decay_t<SignalArgs>...>;
        using SlotArgsTuple = std::tuple<std::decay_t<SlotArgs>...>;
        static_assert(Implementation::is_subset_of<SlotArgsTuple, SignalArgsTuple>::value, "slot function parameters and signal parameters do not match");

        Implementation::Address ReceiverAddress(receiver, handler);
        Implementation::Address SenderAddress(sender, event);

        using Handler = typename Event<SignalArgs...>::Handler;
        Handler v_handler;
        v_handler.handler = std::make_shared<Implementation::EventHandler<U, std::tuple<SlotArgs...>, SignalArgs...>>((U *) receiver, handler);
        v_handler.type = type;
        v_handler.id = std::this_thread::get_id();

        constexpr bool is_object_v = Implementation::is_object<T, std::thread::id>::value && Implementation::is_object<U, std::thread::id>::value;
        constexpr bool is_not_object_v = !Implementation::is_object<T, std::thread::id>::value && !Implementation::is_object<U, std::thread::id>::value;
        static_assert(is_object_v || is_not_object_v, "Sender and Receiver must both be Object or neither be");
        if constexpr (is_object_v)
        {
            v_handler.id = receiver->ThreadId();
            sender->AddReceiver(ReceiverAddress, [=]()
            {
                receiver->RemoveSender(SenderAddress, ReceiverAddress.function);
            });
            receiver->AddSender(SenderAddress, ReceiverAddress.function, [=]()
            {
                (static_cast<T *>(sender)->*event).RemoveHandler(ReceiverAddress);
                sender->RemoveReceiver(ReceiverAddress);
            });
        }

        (static_cast<T *>(sender)->*event).AddHandler(ReceiverAddress, v_handler);
    }

    template<typename Sender, typename Receiver, typename T, typename U, typename ...SignalArgs, typename ...SlotArgs>
    inline constexpr void disconnect(Sender *sender, Event<SignalArgs...> T::* event, Receiver *receiver, void(U::* handler)(SlotArgs...) const)
    {
        Implementation::Address ReceiverAddress(receiver, handler);
        Implementation::Address SenderAddress(sender, event);
        (static_cast<T *>(sender)->*event).RemoveHandler(ReceiverAddress);

        constexpr bool is_object_v = Implementation::is_object<T, std::thread::id>::value && Implementation::is_object<U, std::thread::id>::value;
        constexpr bool is_not_object_v = !Implementation::is_object<T, std::thread::id>::value && !Implementation::is_object<U, std::thread::id>::value;
        static_assert(is_object_v || is_not_object_v, "Sender and Receiver must both be Object or neither be");
        if constexpr (is_object_v)
        {
            if (sender->ContainReceiver(ReceiverAddress) && receiver->ContainSender(SenderAddress))
            {
                sender->RemoveReceiver(ReceiverAddress);
                receiver->RemoveSender(SenderAddress, ReceiverAddress.function);
            }
        }
    }

    template<typename Sender, typename T, typename ...SignalArgs, typename ...SlotArgs>
    inline constexpr void connect(Sender *sender, Event<SignalArgs...> T::* event, void(*handler)(SlotArgs...))
    {
        using SignalArgsTuple = std::tuple<std::decay_t<SignalArgs>...>;
        using SlotArgsTuple = std::tuple<std::decay_t<SlotArgs>...>;
        static_assert(Implementation::is_subset_of<SlotArgsTuple, SignalArgsTuple>::value, "slot function parameters and signal parameters do not match");

        using Handler = typename Event<SignalArgs...>::Handler;
        Handler v_handler
        {
            std::this_thread::get_id(),
            std::make_shared<Implementation::EventHandler<void, std::tuple<SlotArgs...>, SignalArgs...>>(handler),
            SIGSLOT::DirectConnection,
        };
        (static_cast<T *>(sender)->*event).AddHandler(Implementation::Address(handler), v_handler);
    }

    template<typename Sender, typename T, typename ...SignalArgs, typename ...SlotArgs>
    inline constexpr void disconnect(Sender *sender, Event<SignalArgs...> T::* event, void(*handler)(SlotArgs...))
    {
        using SignalArgsTuple = std::tuple<std::decay_t<SignalArgs>...>;
        using SlotArgsTuple = std::tuple<std::decay_t<SlotArgs>...>;
        static_assert(Implementation::is_subset_of<SlotArgsTuple, SignalArgsTuple>::value, "slot function parameters and signal parameters do not match");

        (static_cast<T *>(sender)->*event).RemoveHandler(Implementation::Address(handler));
    }

    template<typename Sender, typename Receiver, typename T, typename Lambda, typename ...SignalArgs>
    inline constexpr void connect(Sender *sender, Event<SignalArgs...> T::* event, Receiver *receiver, Lambda &&lambda, SIGSLOT type)
    {
        using SignalArgsTuple = std::tuple<std::decay_t<SignalArgs>...>;
        using SlotArgsTuple = typename Implementation::lambda_traits<decltype(&Lambda::operator())>::parameter_type;
        static_assert(Implementation::is_subset_of<SlotArgsTuple, SignalArgsTuple>::value, "slot function parameters and signal parameters do not match");

        constexpr bool is_object_v = Implementation::is_object<T, std::thread::id>::value && Implementation::is_object<Receiver, std::thread::id>::value;
        constexpr bool is_not_object_v = !Implementation::is_object<T, std::thread::id>::value && !Implementation::is_object<Receiver, std::thread::id>::value;
        static_assert(is_object_v || is_not_object_v, "Sender and Receiver must both be Object or neither be");

        Implementation::Address SenderAddress(sender, event);
        //借用之前的lambda辅助函数来进行lambda的提取
        auto &&ReceiverAddress = (sender->*event).ImitationFunctionHelper(receiver, lambda, &Lambda::operator(), type);
        if constexpr (is_object_v)
        {
            sender->AddReceiver(ReceiverAddress, [=]()
            {
                receiver->RemoveSender(SenderAddress, ReceiverAddress.function);
            });
            receiver->AddSender(SenderAddress, ReceiverAddress.function, [=]()
            {
                (static_cast<T *>(sender)->*event).RemoveHandler(ReceiverAddress);
                sender->RemoveReceiver(ReceiverAddress);
            });
        }
    }

    inline EventLoop *GetEventLoop(std::thread::id id)
    {
        return Implementation::EventLoopManager::GetInstance()->GetEventLoop(id);
    }

    inline EventLoop *GetEventLoop()
    {
        return GetEventLoop(std::this_thread::get_id());
    }

    inline EventLoop *GetEventLoop(const Thread &thread)
    {
        return GetEventLoop(thread.GetID());
    }

    inline EventLoop *GetEventLoop(const std::thread &thread)
    {
        return GetEventLoop(thread.get_id());
    }
}
namespace SigSlot::Implementation
{
    template<typename Callable>
    inline void AddSender(Object &object, const Address &senderAddress, const Implementation::SIGSLOT_ClassFunctionPtr &functionAddress, Callable &&func)
    {
        object.AddSender(senderAddress, functionAddress, std::forward<Callable>(func));
    }

    template<typename Callable>
    inline void AddReceiver(Object &object, const Address &receiverAddress, Callable &&func)
    {
        object.AddReceiver(receiverAddress, std::forward<Callable>(func));
    }

    inline void RemoveSender(Object &object, const Address &senderAddress, const Implementation::SIGSLOT_ClassFunctionPtr &functionAddress)
    {
        object.RemoveSender(senderAddress, functionAddress);
    }

    inline void RemoveReceiver(Object &object, const Address &address)
    {
        object.RemoveReceiver(address);
    }

    inline bool ContainSender(const Object &object, const Address &address)
    {
        return object.ContainSender(address);
    }

    inline bool ContainReceiver(const Object &object, const Address &address)
    {
        return object.ContainReceiver(address);
    }

}
#define SIGSLOT_OBJECT \
    private:\
    SigSlot::Object SIGSLOT__Object;\
    public:\
        template<typename Callable>\
        void AddSender(const SigSlot::Implementation::Address &SIGSLOT_senderAddress, const SigSlot::Implementation::SIGSLOT_ClassFunctionPtr &SIGSLOT_functionAddress, Callable &&SIGSLOT_func)\
        {\
            SigSlot::Implementation::AddSender(SIGSLOT__Object,SIGSLOT_senderAddress,SIGSLOT_functionAddress,std::forward<Callable>(SIGSLOT_func));\
        }\
\
        template<typename Callable>\
        void AddReceiver(const SigSlot::Implementation::Address &SIGSLOT_receiverAddress, Callable &&SIGSLOT_func)\
        {\
            SigSlot::Implementation::AddReceiver(SIGSLOT__Object,SIGSLOT_receiverAddress,std::forward<Callable>(SIGSLOT_func));\
        }\
\
        void RemoveSender(const SigSlot::Implementation::Address &SIGSLOT_senderAddress, const SigSlot::Implementation::SIGSLOT_ClassFunctionPtr &SIGSLOT_functionAddress)\
        {\
            SigSlot::Implementation::RemoveSender(SIGSLOT__Object,SIGSLOT_senderAddress,SIGSLOT_functionAddress);\
        }\
\
        void RemoveReceiver(const SigSlot::Implementation::Address &SIGSLOT_address)\
        {\
            SigSlot::Implementation::RemoveReceiver(SIGSLOT__Object,SIGSLOT_address);\
        }\
\
        bool ContainSender(const SigSlot::Implementation::Address &SIGSLOT_address) const\
        {\
            return SigSlot::Implementation::ContainSender(SIGSLOT__Object,SIGSLOT_address);\
        }\
\
        bool ContainReceiver(const SigSlot::Implementation::Address &SIGSLOT_address) const\
        {\
            return SigSlot::Implementation::ContainReceiver(SIGSLOT__Object,SIGSLOT_address);\
        }\
    public:\
        void MoveToThread(const std::thread &SIGSLOT_target) noexcept\
        {\
            SIGSLOT__Object.MoveToThread(SIGSLOT_target.get_id());\
        }\
\
        void MoveToThread(const std::thread::id &SIGSLOT_id) noexcept\
        {\
            SIGSLOT__Object.MoveToThread(SIGSLOT_id);\
        }\
\
        void MoveToThread(const SigSlot::Thread &SIGSLOT_other) noexcept\
        {\
            SIGSLOT__Object.MoveToThread(SIGSLOT_other.GetID());\
        }\
\
        std::thread::id ThreadId() noexcept\
        {\
            return SIGSLOT__Object.ThreadId();\
        }\
    private:
#endif //EVENT_SIGSLOT_HPP
